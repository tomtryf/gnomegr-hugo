---
id: 2579
title: Γλωσσάρι
date: 2014-06-05T15:58:08+00:00
author: thanos
layout: page
guid: http://gnome.gr/?page_id=2579
---
<div class="name_directory_column name_directory_nr3">
<div class="name_directory_name_box"><a name="namedirectory_abort"></a>**abort**  

<div>εγκατάλειψη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_about"></a>**about**  

<div>περί</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_accept"></a>**accept**  

<div>αποδοχή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_access"></a>**access**  

<div>πρόσβαση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_accessibility"></a>**accessibility**  

<div>προσιτότητα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_action"></a>**action**  

<div>ενέργεια</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_activate"></a>**activate**  

<div>ενεργοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_active"></a>**active**  

<div>ενεργό, ενεργοποιημένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_add"></a>**add**  

<div>προσθήκη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_addressbook"></a>**addressbook**  

<div>βιβλίο διευθύνσεων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_advanced"></a>**advanced**  

<div>για προχωρημένους</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_album"></a>**album**  

<div>άλμπουμ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_alert"></a>**alert**  

<div>αλγόριθμος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_align"></a>**align**  

<div>στοίχιση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_allow"></a>**allow**  

<div>να επιτρέπεται, επιτρέπεται</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_alphachannel"></a>**alpha channel**  

<div>κανάλι άλφα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_alphavalue"></a>**alpha value**  

<div>τιμή άλφα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Analog"></a>**Analog**  

<div>Αναλογικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_animation"></a>**animation**  

<div>κινούμενη εικόνα, κινούμενο σχέδιο, εφέ κίνησης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Anonymous"></a>**Anonymous**  

<div>Ανώνυμος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Antialiasing"></a>**Antialiasing**  

<div>Εξομάλυνση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_applet"></a>**applet**  

<div>μικροεφαρμογή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_application"></a>**application**  

<div>εφαρμογή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_appointment"></a>**appointment**  

<div>ραντεβού</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_array"></a>**array**  

<div>πίνακας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_ascending"></a>**ascending**  

<div>αύξουσα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Asynchronous"></a>**Asynchronous**  

<div>Ασύγχρονο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_attach"></a>**attach**  

<div>επισύναψη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_attachment"></a>**attachment**  

<div>συνημμένο αρχείο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_attribute"></a>**attribute**  

<div>γνώρισμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_authenticate"></a>**authenticate**  

<div>πιστοποίηση, ταυτοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_authorize"></a>**authorize**  

<div>εξουσιοδότηση, έγκριση (επαφής)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_auto"></a>**auto**  

<div>αυτόματο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_autocomplete"></a>**autocomplete**  

<div>αυτόματη συμπλήρωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_autoformat"></a>**autoformat**  

<div>αυτόματη μορφοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_available"></a>**available**  

<div>διαθέσιμο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_avatar"></a>**avatar**  

<div>προσωπική εικόνα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_back"></a>**back**  

<div>πίσω</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_backend"></a>**backend**  

<div>παρασκήνιο, σύστημα υποστήριξης (για κάποια εφαρμογή)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_background"></a>**background**  

<div>background φόντο (επιφάνειας εργασίας), παρασκήνιο (εκτέλεση εργασιών στο παρασκήνιο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Backlink"></a>**Backlink**  

<div>Σύνδεσμος επιστροφής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_backslash"></a>**backslash**  

<div>ανάποδη κάθετος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Balance"></a>**Balance**  

<div>Ισορροπία, Υπόλοιπο (π.χ υπόλοιο κάρτας/μονάδων στο ekiga κλπ)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_ban"></a>**ban**  

<div>αποκλεισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bandwidth"></a>**bandwidth**  

<div>εύρος ζώνης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bcc"></a>**bcc**  

<div>κρυφή κοινοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_beep"></a>**beep**  

<div>ηχητικό σήμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Benchmark"></a>**Benchmark**  

<div>Έλεγχος επιδόσεων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Binary"></a>**Binary**  

<div>Δυαδικό, Εκτελέσιμο (αν έχουμε να κάνουμε με πρόγραμμα)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bit"></a>**bit**  

<div>bit (π.χ 32-bit, 64-bit)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_blank"></a>**blank**  

<div>κενό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_blog"></a>**blog**  

<div>ιστολόγιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bold"></a>**bold**  

<div>έντονα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bond"></a>**bond**  

<div>δεσμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bookmark"></a>**bookmark**  

<div>σελιδοδείκτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_boolean"></a>**boolean**  

<div>λογικός/ή (τιμή, μεταβλητή)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_boot"></a>**boot**  

<div>εκκίνηση (συστήματος)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_border"></a>**border**  

<div>περίγραμμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_box"></a>**box**  

<div>πλαίσιο, κουτί</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_broker"></a>**broker**  

<div>μεσολαβητής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_browser"></a>**browser**  

<div>περιηγητής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_buffer"></a>**buffer**  

<div>ενδιάμεση μνήμη, φορτώνω</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_buffering"></a>**buffering**  

<div>φόρτωση, φορτώνω</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bug"></a>**bug**  

<div>σφάλμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_build"></a>**build**  

<div>δόμηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bullet"></a>**bullet**  

<div>κουκίδα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_burn"></a>**burn**  

<div>εγγραφή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Bus"></a>**Bus**  

<div>Δίαυλος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Busy"></a>**Busy**  

<div>Απασχολημένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_button"></a>**button**  

<div>κουμπί</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_bytes"></a>**bytes**  

<div>bytes (π.χ 20 bytes)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cache"></a>**cache**  

<div>λανθάνουσα μνήμη (ή μνήμη, αν συνοδεύεται από άλλο προσδιοριστικό)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cached"></a>**cached**  

<div>προσωρινά αποθηκευμένα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_calculator"></a>**calculator**  

<div>αριθμομηχανή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Calendar"></a>**Calendar**  

<div>Ημερολόγιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_calibrate"></a>**calibrate**  

<div>βαθμονομώ, βαθμονόμηση (μια συσκευή)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Call"></a>**Call**  

<div>Κλήση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_camcorder"></a>**camcorder**  

<div>βιντεοκάμερα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_camera"></a>**camera**  

<div>κάμερα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cancel"></a>**cancel**  

<div>ακύρωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_capacity"></a>**capacity**  

<div>χωρητικότητα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_caption"></a>**caption**  

<div>λεζάντα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Categories"></a>**Categories**  

<div>Κατηγορίες</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_CCWcounter-clockwise"></a>**CCW (counter-clockwise)**  

<div>αριστερόστροφα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cell"></a>**cell**  

<div>κελί (σε λογιστικό φύλλο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_certificate"></a>**certificate**  

<div>πιστοποιητικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_certification"></a>**certification**  

<div>πιστοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_channel"></a>**channel**  

<div>κανάλι</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_character"></a>**character**  

<div>χαρακτήρας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_chart"></a>**chart**  

<div>διάγραμμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_checkbox"></a>**checkbox**  

<div>πλαίσιο επιλογής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_checkout"></a>**checkout**  

<div>checkout (σε git,svn κλπ η διαδικασία που κάνει κάποιος για να εξάγει πχ ένα αποθετήριο από τον διακομιστή στον υπολογιστή του)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_childprocess"></a>**child process**  

<div>θυγατρική διεργασία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_clear"></a>**clear**  

<div>εκκαθάριση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_click"></a>**click**  

<div>κάντε κλικ, να κάνετε κλικ (ρήμα), κλικ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Client"></a>**Client**  

<div>Πελάτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_clipboard"></a>**clipboard**  

<div>πρόχειρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Close"></a>**Close**  

<div>Κλείσιμο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_collapse"></a>**collapse**  

<div>σύμπτυξη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_colorpicker"></a>**color picker**  

<div>επιλογέας χρωμάτων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_column"></a>**column**  

<div>στήλη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_combobox"></a>**combo box**  

<div>πλαίσιο επιλογών (και πλαίσιο πολλαπλών επιλογών αν φτάνει ο χώρος και είναι χρήσιμη η διευκρίνιση)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_commandline"></a>**command line**  

<div>γραμμή εντολών</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_commandprompt"></a>**command prompt**  

<div>γραμμή εντολών</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_compatible"></a>**compatible**  

<div>συμβατός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_compile"></a>**compile**  

<div>μεταγλώττιση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_compositing"></a>**compositing**  

<div>σύνθεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_compression"></a>**compression**  

<div>συμπίεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_concatenation"></a>**concatenation**  

<div>συνένωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_confidential"></a>**confidential**  

<div>εμπιστευτικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_confirm"></a>**confirm**  

<div>επιβεβαίωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_conflict"></a>**conflict**  

<div>σύγκρουση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_connection"></a>**connection**  

<div>σύνδεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_console"></a>**console**  

<div>κονσόλα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_constant"></a>**constant**  

<div>σταθερά (μαθηματικά)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_contact"></a>**contact**  

<div>επαφή, επικοινωνία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Content"></a>**Content**  

<div>Περιεχόμενο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_continue"></a>**continue**  

<div>συνέχεια</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_contributors"></a>**contributors**  

<div>συντελεστές</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cookie"></a>**cookie**  

<div>cookie</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Copy"></a>**Copy**  

<div>Αντιγραφή (η ενέργεια), αντίγραφο (το)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Copyright"></a>**Copyright**  

<div>Πνευματικά δικαιώματα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_core"></a>**core**  

<div>βασικός, πυρήνας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_counter"></a>**counter**  

<div>μετρητής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_CPU"></a>**CPU**  

<div>CPU</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_crash"></a>**crash**  

<div>κατάρρευση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_create"></a>**create**  

<div>δημιουργία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_credential"></a>**credential**  

<div>διαπιστευτήριο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_credits"></a>**credits**  

<div>μνεία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Critical"></a>**Critical**  

<div>Κρίσιμο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_crop"></a>**crop**  

<div>περικοπή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_current"></a>**current**  

<div>τρέχον (ή επιλεγμένο, αν ταιριάζει)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cursor"></a>**cursor**  

<div>δρομέας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Curve"></a>**Curve**  

<div>Καμπύλη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_custom"></a>**custom**  

<div>προσαρμοσμένος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_customized"></a>**customized**  

<div>προσαρμοσμένος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cut"></a>**cut**  

<div>αποκοπή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_CWclockwise"></a>**CW (clockwise)**  

<div>δεξιόστροφα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_cyan"></a>**cyan**  

<div>κυανό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_daemon"></a>**daemon**  

<div>υπηρεσία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_database"></a>**database**  

<div>βάση δεδομένων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_debug"></a>**debug**  

<div>αποσφαλμάτωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_decimal"></a>**decimal**  

<div>δεκαδικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_decompression"></a>**decompression**  

<div>αποσυμπίεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_decryption"></a>**decryption**  

<div>αποκρυπτογράφηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_default"></a>**default**  

<div>προεπιλογή, προεπιλεγμένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_delimiter"></a>**delimiter**  

<div>οριοθέτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_dependency"></a>**dependency**  

<div>εξάρτηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_deprecated"></a>**deprecated**  

<div>παρωχημένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_descending"></a>**descending**  

<div>φθίνουσα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_desktop"></a>**desktop**  

<div>επιφάνεια εργασίας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Destination"></a>**Destination**  

<div>Προορισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_device"></a>**device**  

<div>συσκευή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Diagram"></a>**Diagram**  

<div>Διάγραμμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_dial"></a>**dial**  

<div>κλήση (αριθμού), καντράν (συσκευής τηλεφώνου)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_dialogbox"></a>**dialog box**  

<div>παράθυρο διαλόγου</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Digit"></a>**Digit**  

<div>Ψηφίο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Dimension"></a>**Dimension**  

<div>Διάσταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Direction"></a>**Direction**  

<div>Κατεύθυνση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_directory"></a>**directory**  

<div>κατάλογος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_dirty"></a>**dirty**  

<div>τροποποιημένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_disable"></a>**disable**  

<div>απενεργοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_discard"></a>**discard**  

<div>απόρριψη (αλλαγών)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_disconnect"></a>**disconnect**  

<div>αποσύνδεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_disk"></a>**disk**  

<div>δίσκος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_display"></a>**display**  

<div>εμφάνιση (ως ρήμα), οθόνη (η συσκευή)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_distribution"></a>**distribution**  

<div>διανομή (λειτουργικού)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_distro"></a>**distro**  

<div>διανομή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Division"></a>**Division**  

<div>Διαίρεση (μαθηματικά)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_dock"></a>**dock**  

<div>μπάρα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_document"></a>**document**  

<div>έγγραφο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_documentation"></a>**documentation**  

<div>τεκμηρίωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_domain"></a>**domain**  

<div>τομέας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Done"></a>**Done**  

<div>Ολοκληρώθηκε</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_doubleclick"></a>**double click**  

<div>κάντε διπλό κλικ, να κάνετε διπλό κλικ, διπλό κλικ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_download"></a>**download**  

<div>λήψη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_draft"></a>**draft**  

<div>προσχέδιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_drag"></a>**drag**  

<div>σύρετε</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Draganddrop"></a>**Drag and drop**  

<div>Μεταφορά και απόθεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_driver"></a>**driver**  

<div>οδηγός (λογισμικού)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_dropdown"></a>**drop down (κάτι)**  

<div>αναπτυσσόμενο (κάτι)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_dynamic"></a>**dynamic**  

<div>δυναμικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_e-mail"></a>**e-mail**  

<div>ηλεκτρονικό ταχυδρομείο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_edit"></a>**edit**  

<div>επεξεργασία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_editor"></a>**editor**  

<div>επεξεργαστής, συντάκτης (άτομο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_effect"></a>**effect**  

<div>εφέ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_eject"></a>**eject**  

<div>εξαγωγή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_element"></a>**element**  

<div>στοιχείο (html)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_email"></a>**email**  

<div>ηλεκτρονική αλληλογγραφία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_embedded"></a>**embedded**  

<div>ενσωματωμένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_encoding"></a>**encoding**  

<div>κωδικοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_encryption"></a>**encryption**  

<div>κρυπτογράφηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_enduser"></a>**end user**  

<div>τελικός χρήστης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_engine"></a>**engine**  

<div>μηχανή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_enter"></a>**enter**  

<div>enter (πλήκτρο), πληκτρολογώ (ως ρήμα)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_erase"></a>**erase**  

<div>διαγραφή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_error"></a>**error**  

<div>σφάλμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_ESC"></a>**ESC**  

<div>ESC (το πλήκτρο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_escapecharacter"></a>**escape character**  

<div>χαρακτήρας διαφυγής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_estimate"></a>**estimate**  

<div>εκτίμηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_EULA"></a>**EULA**  

<div>άδεια χρήσης τελικού χρήστη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_exception"></a>**exception**  

<div>εξαίρεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_exclamationmark"></a>**exclamation mark**  

<div>θαυμαστικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_executable"></a>**executable**  

<div>εκτελέσιμο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Execute"></a>**Execute**  

<div>Εκτέλεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_exit"></a>**exit**  

<div>έξοδος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_expire"></a>**expire**  

<div>λήξη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_export"></a>**export"**  

<div>thanos</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_expression"></a>**expression**  

<div>έκφραση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_extension"></a>**extension**  

<div>επέκταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_extract"></a>**extract**  

<div>αποσυμπίεση (από αρχείο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_extractor"></a>**extractor**  

<div>εξαγωγέας (π.χ Εφαρμογή εξαγωγής ήχου από μουσικό CD - Audio CD Extractor, Λειτουργία εξαγωγής)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fail"></a>**fail**  

<div>αποτυχία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_FAQ"></a>**FAQ**  

<div>Συχνές ερωτήσεις</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fatalerror"></a>**fatal error**  

<div>μοιραίο σφάλμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_favorite"></a>**favorite**  

<div>αγαπημένα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_features"></a>**feature/s**  

<div>λειτουργία/ες</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_feed"></a>**feed**  

<div>τροφοδοσία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_feedback"></a>**feedback**  

<div>ανάδραση</div>

</div>

* * *

<div class="name_directory_column name_directory_nr3">

<div class="name_directory_name_box"><a name="namedirectory_fetching"></a>**fetching**  

<div>ανάκτηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_field"></a>**field**  

<div>πεδίο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_file"></a>**file**  

<div>αρχείο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_filesystem"></a>**filesystem**  

<div>σύστημα αρχείων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_filter"></a>**filter**  

<div>φιλτράρισμα, φίλτρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_find"></a>**find**  

<div>εύρεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_firmware"></a>**firmware**  

<div>υλικολογισμικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fixed"></a>**fixed**  

<div>σταθερό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_flag"></a>**flag**  

<div>σημαία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_flexible"></a>**flexible**  

<div>ευέλικτο (πρόγραμμα)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_floatingpointnumber"></a>**floating point number**  

<div>αριθμός κινητής υποδιαστολής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_floppy"></a>**floppy**  

<div>δισκέτα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_folder"></a>**folder**  

<div>φάκελος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_font"></a>**font**  

<div>γραμματοσειρά</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_footer"></a>**footer**  

<div>υποσέλιδο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_footnote"></a>**footnote**  

<div>υποσημείωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fork"></a>**fork**  

<div>διακλάδωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_format"></a>**format**  

<div>μορφοποίηση (κειμένου), μορφή (αρχείου)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_forward"></a>**forward**  

<div>προώθηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fragmentation"></a>**fragmentation**  

<div>κατακερματισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_frame"></a>**frame**  

<div>πλαίσιο (σε κινούμενη εικόνα τότε καλύτερα είναι να μεταφραστεί ως καρέ)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_framework"></a>**framework**  

<div>πλαίσιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_frontend"></a>**frontend**  

<div>διεπαφή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fullstop"></a>**full stop**  

<div>τελεία (σημείο στίξης .)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fullscreen"></a>**fullscreen**  

<div>πλήρης οθόνη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_function"></a>**function**  

<div>συνάρτηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_fuzzy"></a>**fuzzy**  

<div>ασαφής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_gallery"></a>**gallery**  

<div>συλλογή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_gamma"></a>**gamma**  

<div>γάμμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_get"></a>**get**  

<div>λήψη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_go"></a>**go**  

<div>μετάβαση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Graph"></a>**Graph**  

<div>Γράφημα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_graphics"></a>**graphics**  

<div>γραφικά</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_grid"></a>**grid**  

<div>πλέγμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_group"></a>**group**  

<div>ομάδα, ομαδοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_guest"></a>**guest**  

<div>επισκέπτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hack"></a>**hack**  

<div>επεμβαίνω, επέμβαση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hacker"></a>**hacker**  

<div>χάκερ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_handler"></a>**handler**  

<div>χειριστής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_HardDisk"></a>**Hard Disk**  

<div>Σκληρός δίσκος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hardware"></a>**hardware**  

<div>υλικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_header"></a>**header**  

<div>κεφαλίδα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_headphones"></a>**headphones**  

<div>ακουστικά</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_height"></a>**height**  

<div>ύψος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_help"></a>**help**  

<div>βοηθώ, βοήθεια (ενός προγράμματος)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hex"></a>**hex**  

<div>δεκαεξαδικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hibernate"></a>**hibernate**  

<div>αδρανοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hidden"></a>**hidden**  

<div>κρυφό (π.χ αρχείο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hide"></a>**hide**  

<div>απόκρυψη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_highlight"></a>**highlight**  

<div>επισήμανση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hint"></a>**hint**  

<div>υπόδειξη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_histogram"></a>**histogram**  

<div>ιστόγραμμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_history"></a>**history**  

<div>ιστορικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_home"></a>**home**  

<div>προσωπικός φάκελος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_horizontal"></a>**horizontal**  

<div>οριζόντιος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_host"></a>**host**  

<div>κεντρικός υπολογιστής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hotkey"></a>**hotkey**  

<div>πλήκτρο ενεργειών</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_hyperlink"></a>**hyperlink**  

<div>υπερσύνδεμος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_icon"></a>**icon**  

<div>εικονίδιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_IDidentifier"></a>**ID (identifier)**  

<div>αναγνωριστικό (ενός προγράμματος,μιας παραμέτρου), ταυτότητα (π.χ να</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_image"></a>**image**  

<div>εικόνα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_implementation"></a>**implementation**  

<div>υλοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_import"></a>**import**  

<div>εισαγωγή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_inbox"></a>**inbox**  

<div>εισερχόμενα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_incoming"></a>**incoming**  

<div>εισερχόμενα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_incompatibility"></a>**incompatibility**  

<div>ασυμβατότητα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_increase"></a>**increase**  

<div>αύξηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_indent"></a>**indent**  

<div>εσοχή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Information"></a>**Information**  

<div>Πληροφορία/ες</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_infrared"></a>**infrared**  

<div>υπέρυθρες</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_inode"></a>**inode**  

<div>κόμβος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_insert"></a>**insert**  

<div>εισαγωγή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_install"></a>**install**  

<div>εγκατάσταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_instantmessage"></a>**instant message**  

<div>άμεσο μήνυμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_integer"></a>**integer**  

<div>ακέραιος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Integration"></a>**Integration**  

<div>Ενσωμάτωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_interface"></a>**interface**  

<div>διεπαφή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Internal"></a>**Internal**  

<div>Εσωτερικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Interrupt"></a>**Interrupt**  

<div>Διακοπή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_invalid"></a>**invalid**  

<div>μη έγκυρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_invert"></a>**invert**  

<div>αντιστροφή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Invisible"></a>**Invisible**  

<div>Αόρατο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_IPAddress"></a>**IP Address**  

<div>Διεύθυνση IP</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_italic"></a>**italic**  

<div>πλάγια</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_item"></a>**item**  

<div>αντικείμενο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Jailbroken"></a>**Jailbroken**  

<div>Ξεκλειδωμένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_job"></a>**job**  

<div>εργασία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_jump"></a>**jump**  

<div>μετάβαση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_kernel"></a>**kernel**  

<div>πυρήνας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_kernelcore"></a>**kernel core**  

<div>εσωτερικός πυρήνας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_key"></a>**key**  

<div>πλήκτρο (πληκτρολόγιο), κλειδί (κρυπτογράφησης)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_kill"></a>**kill**  

<div>βίαιος τερματισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_label"></a>**label**  

<div>ετικέτα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Landscape"></a>**Landscape**  

<div>Τοπίο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Language"></a>**Language**  

<div>Γλώσσα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Laptop"></a>**Laptop**  

<div>Φορητός υπολογιστής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_launch"></a>**launch**  

<div>εκκίνηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_launcher"></a>**launcher**  

<div>εκκινητής</div>

</div></div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_layout"></a>**layout**  

<div>διάταξη (πληκτρολογίου, σελίδας)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_legend"></a>**legend**  

<div>υπόμνημα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Length"></a>**Length**  

<div>Μήκος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Library"></a>**Library**  

<div>Βιβλιοθήκη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_license"></a>**license**  

<div>άδεια χρήσης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_licenseagreement"></a>**license agreement**  

<div>άδεια χρήσης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Line"></a>**Line**  

<div>Γραμμή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_link"></a>**link**  

<div>σύνδεσμος, σύνδεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_linux"></a>**linux**  

<div>linux</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_listbox"></a>**list box**  

<div>πλαίσιο λίστας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_liveupdate"></a>**live update**  

<div>άμεση ενημέρωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_local"></a>**local**  

<div>τοπικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Location"></a>**Location**  

<div>Τοποθεσία (για μέρος), Θέση (ενός αρχείου σε φάκελο, κατάλογο κλπ)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Lock"></a>**Lock**  

<div>Κλείδωμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_log"></a>**log**  

<div>καταγραφή, αρχείο καταγραφής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_loggedin"></a>**logged in**  

<div>συνδεδεμένος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_loggedout"></a>**logged out**  

<div>αποσυνδεδεμένος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_login"></a>**login**  

<div>σύνδεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_logo"></a>**logo**  

<div>λογότυπο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_logout"></a>**logout**  

<div>αποσύνδεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_lowercase"></a>**lowercase**  

<div>πεζά</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_macro"></a>**macro**  

<div>μακροεντολή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_mail"></a>**mail**  

<div>αλληλογραφία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_mailinglist"></a>**mailing list**  

<div>λίστα αλληλογραφίας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_mainwindow"></a>**main window**  

<div>κύριο παράθυρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_malformed"></a>**malformed**  

<div>κακοδιατυπωμένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_man"></a>**man**  

<div>man (τεκμηρίωση)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_margin"></a>**margin**  

<div>περιθώριο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Maximize"></a>**Maximize**  

<div>Μεγιστοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_maximum"></a>**maximum**  

<div>μέγιστος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Media"></a>**Media**  

<div>Πολυμέσα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_meeting"></a>**meeting**  

<div>συνάντηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_memo"></a>**memo**  

<div>σημείωμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Memory"></a>**Memory**  

<div>Μνήμη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Menu"></a>**Menu**  

<div>Μενού</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_merge"></a>**merge**  

<div>συγχώνευση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Message"></a>**Message**  

<div>Μήνυμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_method"></a>**method**  

<div>μέθοδος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_microphone"></a>**microphone**  

<div>μικρόφωνο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_MIMEType"></a>**MIME Type**  

<div>Τύπος MIME</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_miner"></a>**miner**  

<div>εξορύκτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Minimize"></a>**Minimize**  

<div>Ελαχιστοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_minimum"></a>**minimum**  

<div>ελάχιστος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_mixer"></a>**mixer**  

<div>μείκτης (ήχου, εικόνας)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_mobilebroadband"></a>**mobile broadband**  

<div>κινητή ευρυζωνική (σύνδεση)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_modoperator"></a>**mod operator**  

<div>τελεστής υπολοίπου</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_mode"></a>**mode**  

<div>λειτουργία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Modem"></a>**Modem**  

<div>Μόντεμ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_moderator"></a>**moderator**  

<div>συντονιστής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_modifier"></a>**modifier**  

<div>τροποποιητής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Modify"></a>**Modify**  

<div>Τροποποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_module"></a>**module**  

<div>άρθρωμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_modulusoperator"></a>**modulus operator**  

<div>τελεστής υπολοίπου</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Monitor"></a>**Monitor**  

<div>Οθόνη, Παρακολουθώ, Παρακολούθηση (π.χ πρόγραμμα παρακολούθησης - monitoring application)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Mount"></a>**Mount**  

<div>Προσάρτηση (συσκευής)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Move"></a>**Move**  

<div>Μετακίνηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_namespaces"></a>**namespaces**  

<div>χώροι ονομάτων, ονοματοθεσίες (όρος προγραμματισμού)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Network"></a>**Network**  

<div>Δίκτυο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_New"></a>**New**  

<div>Νέο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Nightly"></a>**Nightly**  

<div>Πειραματικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Notebook"></a>**Notebook**  

<div>Φορητός υπολογιστής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_notificationarea"></a>**notification area**  

<div>περιοχή ειδοποιήσεων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_object"></a>**object**  

<div>αντικείμενο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_object-oriented"></a>**object-oriented**  

<div>αντικειμενοστραφής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_obsolete"></a>**obsolete**  

<div>παρωχημένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_offline"></a>**offline**  

<div>αποσυνδεδεμένος (ή εκτός σύνδεσης)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_ok"></a>**ok**  

<div>εντάξει</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_on-the-fly"></a>**on-the-fly**  

<div>απευθείας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_online"></a>**online**  

<div>συνδεδεμένος, διαδικτυακός (π.χ διαδικτυακή εφαρμογή - online application)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Open"></a>**Open**  

<div>Άνοιγμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_OpenSource"></a>**Open Source**  

<div>Ανοιχτού κώδικα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_operand"></a>**operand**  

<div>τελεστέος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_operatingsystem"></a>**operating system**  

<div>λειτουργικό σύστημα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_operator"></a>**operator**  

<div>τελεστής (μαθηματικά)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_optimal"></a>**optimal**  

<div>βέλτιστος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_optional"></a>**optional**  

<div>προαιρετικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_options"></a>**options**  

<div>επιλογές</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_orientation"></a>**orientation**  

<div>προσανατολισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_outgoing"></a>**outgoing**  

<div>εξερχόμενα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_output"></a>**output**  

<div>έξοδος (πηγή εξόδου), προϊόν/αποτέλεσμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_overview"></a>**overview**  

<div>επισκόπηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_owner"></a>**owner**  

<div>ιδιοκτήτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_package"></a>**package**  

<div>πακέτο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Packet"></a>**Packet**  

<div>Πακέτο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Page"></a>**Page**  

<div>Σελίδα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_pane"></a>**pane**  

<div>παράθυρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Panel"></a>**Panel**  

<div>Πίνακας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_parameter"></a>**parameter**  

<div>παράμετρος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_parent"></a>**parent**  

<div>αρχικός, γονικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_parsing"></a>**parsing**  

<div>ανάλυση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_partition"></a>**partition**  

<div>κατάτμηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_passphrase"></a>**passphrase**  

<div>συνθηματικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Password"></a>**Password**  

<div>Κωδικός πρόσβασης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_paste"></a>**paste**  

<div>επικόλληση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_patch"></a>**patch**  

<div>διόρθωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_path"></a>**path**  

<div>διαδρομή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_pattern"></a>**pattern**  

<div>μοτίβο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_peer"></a>**peer**  

<div>ομότιμος (http://p2pfoundation.net/Greek_language)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_pending"></a>**pending**  

<div>Εκκρεμεί</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_permissions"></a>**permissions**  

<div>δικαιώματα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_PIN"></a>**PIN**  

<div>PIN (για κλειδί ξεκλειδώματος)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_pinned"></a>**pinned**  

<div>καρφιτσωμένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_pipeline"></a>**pipeline**  

<div>διοχέτευση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Pixel"></a>**Pixel**  

<div>Εικονοστοιχείο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_play"></a>**play**  

<div>αναπαραγωγή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_playlist"></a>**playlist**  

<div>λίστα αναπαραγωγής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_plugin"></a>**plugin**  

<div>πρόσθετο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_podcast"></a>**podcast**  

<div>φορητές εκπομπές (φορητές εκπομπές (podcast) - αν μας το επιτρέπει ο χώρος)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_pointer"></a>**pointer**  

<div>δρομέας (για το ποντίκι), δείκτης (στον προγραμματισμό)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_policy"></a>**policy**  

<div>πολιτική</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_pop-upwindow"></a>**pop-up window**  

<div>αναδυόμενο παράθυρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_popup"></a>**popup**  

<div>αναδυόμενο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_port"></a>**port**  

<div>θύρα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_preferences"></a>**preferences**  

<div>προτιμήσεις</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Prefix"></a>**Prefix**  

<div>Πρόθεμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_presentation"></a>**presentation**  

<div>παρουσίαση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_preview"></a>**preview**  

<div>προεπισκόπηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_priority"></a>**priority**  

<div>προτεραιότητα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Privacy"></a>**Privacy**  

<div>Απόρρητο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_privilege"></a>**privilege**  

<div>προνόμιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_process"></a>**process**  

<div>διεργασία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Processor"></a>**Processor**  

<div>Επεξεργαστής (υπολογιστή, κειμένου)</div>

</div>

* * *

</div>

<div class="name_directory_column name_directory_nr3">

<div class="name_directory_name_box"><a name="namedirectory_Profil"></a>**Profil**  

<div>Προφίλ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_program"></a>**program**  

<div>πρόγραμμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Progress"></a>**Progress**  

<div>Πρόοδος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_ProgressBar"></a>**Progress Bar**  

<div>Γραμμή προόδου</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_project"></a>**project**  

<div>έργο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_property"></a>**property**  

<div>ιδιότητα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_proprietarysoftware"></a>**proprietary software**  

<div>ιδιοταγές λογισμικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Protected"></a>**Protected**  

<div>Προστατευμένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Protocol"></a>**Protocol**  

<div>Πρωτόκολλο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_provider"></a>**provider**  

<div>πάροχος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_proxy"></a>**proxy**  

<div>διαμεσολαβητής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Public"></a>**Public**  

<div>Δημόσιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_publish"></a>**publish**  

<div>έκδοση, δημοσιεύω/δημοσίευση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Quit"></a>**Quit**  

<div>Έξοδος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_quotedtext"></a>**quoted text**  

<div>κείμενο σε παράθεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_radiobutton"></a>**radio button**  

<div>κουμπί επιλογής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_RAM"></a>**RAM**  

<div>RAM</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_RandomAccessMemory"></a>**Random Access Memory**  

<div>Μνήμη τυχαίας προσπέλασης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Ratio"></a>**Ratio**  

<div>Αναλογία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_read-only"></a>**read-only**  

<div>μόνο-ανάγνωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_ready"></a>**ready**  

<div>έτοιμος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_recipient"></a>**recipient**  

<div>παραλήπτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_recognition"></a>**recognition**  

<div>αναγνώριση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_record"></a>**record**  

<div>εγγραφή (ήχου, πίνακα/καταλόγου)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_recover"></a>**recover**  

<div>ανάκτηση (αρχείου), επαναφορά (υπολογιστή)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_rectangle"></a>**rectangle**  

<div>ορθογώνιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_recursive"></a>**recursive**  

<div>αναδρομικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_redirect"></a>**redirect**  

<div>ανακατεύθυνση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_redo"></a>**redo**  

<div>επανάληψη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_release"></a>**release**  

<div>έκδοση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_ReleaseNotes"></a>**Release Notes**  

<div>Σημειώσεις έκδοσης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_remote"></a>**remote**  

<div>απομακρυσμένο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_remove"></a>**remove**  

<div>αφαίρεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Rename"></a>**Rename**  

<div>Μετονομασία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_rendering"></a>**rendering**  

<div>απεικόνιση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_repeat"></a>**repeat**  

<div>επανάληψη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Replace"></a>**Replace**  

<div>Αντικατάσταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_reply"></a>**reply**  

<div>απάντηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Reports"></a>**Reports**  

<div>Αναφορές</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_repository"></a>**repository**  

<div>αποθετήριο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_request"></a>**request**  

<div>αίτηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_requested"></a>**requested**  

<div>ζητούμενο, ζητούμενη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_restart"></a>**restart**  

<div>επανεκκίνηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_rewind"></a>**rewind**  

<div>πισωγύρισμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_roaming"></a>**roaming**  

<div>περιαγωγή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_room"></a>**room**  

<div>δωμάτιο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_root"></a>**root**  

<div>root (ο χρήστης), ριζικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_rotate"></a>**rotate**  

<div>περιστροφή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_router"></a>**router**  

<div>δρομολογητής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_row"></a>**row**  

<div>γραμμή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_rule"></a>**rule**  

<div>κανόνας (ή χάρακας αναλόγως το νόημα της πρότασης)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_run"></a>**run**  

<div>εκτέλεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_runtime"></a>**runtime**  

<div>χρόνος εκτέλεσης, περιβάλλον εκτέλεσης (π.χ python runtime)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_sample"></a>**sample**  

<div>δείγμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_saturation"></a>**saturation**  

<div>κορεσμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_save"></a>**save**  

<div>αποθήκευση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_scan"></a>**scan**  

<div>σάρωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_screen"></a>**screen**  

<div>οθόνη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_screensaver"></a>**screensaver**  

<div>προστασία οθόνης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_screenshot"></a>**screenshot**  

<div>στιγμιότυπο (οθόνης)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_script"></a>**script**  

<div>δέσμη ενεργειών</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_scroll"></a>**scroll**  

<div>κύλιση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_search"></a>**search**  

<div>αναζήτηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_searchengine"></a>**search engine**  

<div>μηχανή αναζήτησης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_sector"></a>**sector**  

<div>τομέας (π.χ σκληρού δίσκου)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_secure"></a>**secure**  

<div>ασφαλής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_security"></a>**security**  

<div>ασφάλεια</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_seek"></a>**seek**  

<div>αναζήτηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_server"></a>**server**  

<div>διακομιστής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_session"></a>**session**  

<div>συνεδρία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_settings"></a>**settings**  

<div>ρυθμίσεις</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_share"></a>**share**  

<div>κοινή χρήση, διαμοιρασμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_shared"></a>**shared**  

<div>κοινόχρηστο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_shortcut"></a>**shortcut**  

<div>συντόμευση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_show"></a>**show**  

<div>εμφάνιση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_signal"></a>**signal**  

<div>σήμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_sink"></a>**sink**  

<div>δέκτης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_slot"></a>**slot**  

<div>θέση, υποδοχή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_softdependences"></a>**soft dependences**  

<div>χαλαρές εξαρτήσεις</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_software"></a>**software**  

<div>λογισμικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_sort"></a>**sort**  

<div>ταξινόμηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_source"></a>**source**  

<div>πηγή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_sourcecode"></a>**source code**  

<div>πηγαίος κώδικας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_space"></a>**space**  

<div>διάστημα (πλήκτρο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_spacebar"></a>**spacebar**  

<div>πλήκτρο διαστήματος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_spacing"></a>**spacing**  

<div>απόσταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_spam"></a>**spam**  

<div>ανεπιθύμητα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_specifications"></a>**specifications**  

<div>προδιαγραφές</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_spellcheck"></a>**spellcheck**  

<div>ορθογραφικός έλεγχος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_split"></a>**split**  

<div>διαχωρίζω, διαχωρισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_splitter"></a>**splitter**  

<div>διαχωριστής (δίκτυα splitter)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_stack"></a>**stack**  

<div>στοίβα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Standard"></a>**Standard**  

<div>Τυπικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_state"></a>**state**  

<div>κατάσταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_stateful"></a>**stateful**  

<div>με επίβλεψη κατάστασης, με καταστάσεις</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_status"></a>**status**  

<div>κατάσταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_stdin"></a>**stdin**  

<div>τυπική είσοδος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_stdout"></a>**stdout**  

<div>τυπική έξοδος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_stream"></a>**stream**  

<div>ροή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_strikethrough"></a>**strikethrough**  

<div>επιγράμμιση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_String"></a>**String**  

<div>Συμβολοσειρά</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_style"></a>**style**  

<div>στιλ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_stylesheets"></a>**stylesheets**  

<div>φύλλα στυλ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_subject"></a>**subject**  

<div>θέμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_submenu"></a>**submenu**  

<div>υπομενού</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_submit"></a>**submit**  

<div>υποβολή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_subscriber"></a>**subscriber**  

<div>συνδρομητής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_suite"></a>**suite**  

<div>σουίτα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_superuser"></a>**superuser**  

<div>υπερχρήστης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_support"></a>**support**  

<div>υποστήριξη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_suspend"></a>**suspend**  

<div>αναστολή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_swap"></a>**swap**  

<div>εναλλαγή (δεν χρησιμοποιείτε για τo swap partition/space)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_swapspace"></a>**swap space**  

<div>χώρος swap (αφορά κατατμήσεις). Σκέτο swap αν αφορά την κατάτμηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_switch"></a>**switch**  

<div>διακόπτης (ουσιαστικό), εναλλαγή (ρήμα)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_sync"></a>**sync**  

<div>συγχρονισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_synchronization"></a>**synchronization**  

<div>συγχρονισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_System"></a>**System**  

<div>Σύστημα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_systemtray"></a>**system tray**  

<div>περιοχή ειδοποιήσεων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tab"></a>**tab**  

<div>καρτέλα (διαλόγου, περιηγητή), στηλοθέτης (σύνταξη κειμένου)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Table"></a>**Table**  

<div>Πίνακας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tablet"></a>**tablet**  

<div>tablet</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tag"></a>**tag**  

<div>ετικέτα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_target"></a>**target**  

<div>προορισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_task"></a>**task**  

<div>εργασία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_taskbar"></a>**taskbar**  

<div>γραμμή εργασιών</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_temp"></a>**temp**  

<div>προσωρινός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_template"></a>**template**  

<div>πρότυπο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_temporary"></a>**temporary**  

<div>προσωρινός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_term"></a>**term**  

<div>όρος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_terminal"></a>**terminal**  

<div>τερματικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_terminate"></a>**terminate**  

<div>τερματισμός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_text"></a>**text**  

<div>κείμενο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_textarea"></a>**text area**  

<div>περιοχή κειμένου</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_textbox"></a>**text box**  

<div>πλαίσιο κειμένου</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_theme"></a>**theme**  

<div>θέμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_thread"></a>**thread**  

<div>νήμα (για θέματα πληροφορικής), αλληλουχία μηνυμάτων (για</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_thumbnail"></a>**thumbnail**  

<div>μικρογραφία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tick"></a>**tick**  

<div>υποδιαίρεση, επιλογή (ανάλογα την πρόταση)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tile"></a>**tile**  

<div>σε παράθεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_timer"></a>**timer**  

<div>χρονόμετρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_timestamp"></a>**timestamp**  

<div>χρονική σήμανση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_timezone"></a>**timezone**  

<div>ζώνη ώρας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tip"></a>**tip**  

<div>συμβουλή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_token"></a>**token**  

<div>διακριτικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tool"></a>**tool**  

<div>εργαλείο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_toolbar"></a>**toolbar**  

<div>εργαλειοθήκη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_toolbox"></a>**toolbox**  

<div>εργαλειοθήκη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_topic"></a>**topic**  

<div>θέμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_total"></a>**total**  

<div>σύνολο/άθροισμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_touchpad"></a>**touchpad**  

<div>πινακίδα αφής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_touchscreen"></a>**touchscreen**  

<div>οθόνη αφής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_track"></a>**track**  

<div>κομμάτι (σε CD), τροχιά (στο σκληρό δίσκο)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tracker"></a>**tracker**  

<div>ανιχνευτής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_transaction"></a>**transaction**  

<div>συναλλαγή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_transfer"></a>**transfer**  

<div>μεταφορά</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_transparency"></a>**transparency**  

<div>διαφάνεια</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_trash"></a>**trash**  

<div>απορρίμματα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tree"></a>**tree**  

<div>δέντρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_trusted"></a>**trusted**  

<div>έμπιστος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_tunneling"></a>**tunneling**  

<div>διοχέτευση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_unattended"></a>**unattended**  

<div>αφύλακτο (πχ unattended updates)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_uncheck"></a>**uncheck**  

<div>αποεπιλογή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_undefined"></a>**undefined**  

<div>ακαθόριστο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_underscore"></a>**underscore**  

<div>υπογράμμιση, χαρακτήρας υπογράμμισης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_undo"></a>**undo**  

<div>αναίρεση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_uninstall"></a>**uninstall**  

<div>απεγκατάσταση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Unity"></a>**Unity**  

<div>Unity (γραφικό περιβάλλον του Ubuntu)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_unlock"></a>**unlock**  

<div>ξεκλείδωμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_unmount"></a>**unmount**  

<div>αποπροσάρτηση (συσκευής)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_update"></a>**update**  

<div>ενημέρωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_upgrade"></a>**upgrade**  

<div>αναβάθμιση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_upload"></a>**upload**  

<div>αποστολή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_uppercase"></a>**uppercase**  

<div>κεφαλαία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_URI"></a>**URI**  

<div>URI</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_URL"></a>**URL**  

<div>διεύθυνση (αν δεν χωράει στο παράθυρο σαν μήνυμα το αφήνουμε URL)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_user"></a>**user**  

<div>χρήστης</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_username"></a>**username**  

<div>όνομα χρήστη</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_utility"></a>**utility**  

<div>εργαλείο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_valid"></a>**valid**  

<div>έγκυρος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_validation"></a>**validation**  

<div>επικύρωση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_value"></a>**value**  

<div>τιμή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_variable"></a>**variable**  

<div>μεταβλητή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_vector"></a>**vector**  

<div>διάνυσμα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_vendor"></a>**vendor**  

<div>κατασκευαστής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_verbose"></a>**verbose**  

<div>αναλυτικό</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_verbosemode"></a>**verbose mode**  

<div>λειτουργία εμφάνισης λεπτομερειών</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_version"></a>**version**  

<div>έκδοση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_versioncontrol"></a>**version control**  

<div>σύστημα ελέγχου εκδόσεων (π.χ τέτοιο σύστημα είναι το Git,Svn,Mercurial κλπ)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_vertical"></a>**vertical**  

<div>κατακόρυφος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_view"></a>**view**  

<div>προβολή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_viewport"></a>**viewport**  

<div>περιοχή προβολής</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_virtual"></a>**virtual**  

<div>εικονικός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_virtualmachine"></a>**virtual machine**  

<div>εικονική μηχανή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Virtualization"></a>**Virtualization**  

<div>Εικονικοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_visible"></a>**visible**  

<div>ορατός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_volume"></a>**volume**  

<div>τόμος (προσαρτημένος), ένταση (ήχου)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_wallpaper"></a>**wallpaper**  

<div>ταπετσαρία</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_warning"></a>**warning**  

<div>προειδοποίηση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_WARNING"></a>**WARNING!**  

<div>ΠΡΟΣΟΧΗ!</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_watermark"></a>**watermark**  

<div>υδατογράφημα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Web"></a>**Web**  

<div>Ιστός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_webcam"></a>**webcam**  

<div>δικτυακή κάμερα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_webpage"></a>**webpage**  

<div>ιστοσελίδα</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_website"></a>**website**  

<div>ιστότοπος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Wi-Fi"></a>**Wi-Fi**  

<div>Wi-Fi</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_widget"></a>**widget**  

<div>γραφικό στοιχείο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_width"></a>**width**  

<div>πλάτος</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_wildcard"></a>**wildcard**  

<div>μπαλαντέρ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Window"></a>**Window**  

<div>Παράθυρο</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_WindowManager"></a>**Window Manager**  

<div>Διαχειριστής παραθύρων</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_wizard"></a>**wizard**  

<div>βοηθός</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_workspace"></a>**workspace**  

<div>χώρος εργασίας</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_wrap"></a>**wrap**  

<div>αναδίπλωση (κειμένου)</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_write"></a>**write**  

<div>εγγραφή</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Xcoordinate"></a>**X coordinate**  

<div>άξονας Χ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Ycoordinate"></a>**Y coordinate**  

<div>άξονας Υ</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Zoom"></a>**Zoom**  

<div>Εστίαση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Zoomin"></a>**Zoom in**  

<div>Μεγέθυνση</div>

</div>

* * *

<div class="name_directory_name_box"><a name="namedirectory_Zoomout"></a>**Zoom out**  

<div>Σμίκρυνση</div>

</div>

</div>
</div>
