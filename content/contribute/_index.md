---
id: 2267
title: Συμμετοχή
date: 2014-04-10T15:20:53+00:00
author: thanos
layout: page
guid: http://gnome.gr/?page_id=2267
---
<div class="text-center">
    <div class="col-sm-4"><a href="#register"><img style="max-height: 96px;" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/user_register.png">Εγγραφή χρήστη</a></div>
    <div class="col-sm-4"><a href="#translate"><img style="max-height: 96px;" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_steps.png">Βήματα μετάφρασης</a></div>
    <div class="col-sm-4"><a href="#bugs"><img style="max-height: 96px;" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2015/03/translation_bug.png">Aναφορά σφαλμάτων</a></div>
</div>

H ελληνική ομάδα του GNOME με ευχαρίστηση δέχεται νέες συμμετοχές στην προσπάθεια της να παρουσιάζεται η κάθε καινούρια έκδοση του GNOME πλήρης όσο αφορά την ελληνική υποστήριξη.

Υπάρχουν βέβαια και άλλα πεδία δράσης εκτός των μεταφράσεων που μπορεί κάποιος να συμμετάσχει,όπως:

*   η συγγραφή κειμένων προώθησης του GNOME στην ιστοσελίδα μας,
*   ο σχεδιασμός artwork για παρουσιάσεις, release parties, συνέδρια κ.λπ,
*   ο έλεγχος των μεταφράσεων και η [αναφορά σφαλμάτων](http://gnome.gr/contribute/#bugs)

Για αυτό το σκοπό ως πρώτο βήμα επικοινωνίας και συνεργασίας δημιουργήσαμε την λίστα ηλεκτρονικού ταχυδρομείου team-gnome.gr από όπου μπορούν οι νέοι και οι παλιοί συμμετέχοντες να συντονίσουν την δράση τους και να ορίσουν τις εργασίες που πρέπει να γίνουν. Εγγραφείτε λοιπόν στην [λίστα gnome-el](https://mail.gnome.org/mailman/listinfo/gnome-el-list) και ξεκινήστε την συμμετοχή σας στην κοινότητα του Ελληνικού GNOME.

Ως επόμενο βήμα για τον καλύτερο συντονισμό των εργασιών,δημιουργήσαμε ένα χώρο όπου θα λαμβάνει μέρος η ομαδική δουλειά της κοινότητας. Ρίξτε μια ματιά στο [αποθετηρίο](https://github.com/gnomegr) της ελληνικής κοινότητας στο GitHub.

* * *

<a name="register">

## Εγγραφή χρήστη

</a>

<a name="register"></a>

1.  Εγγραφή στην [ιστοσελίδα διαχείρισης των μεταφράσεων του GNOME (Damned Lies)](http://l10n.gnome.org/register/)

<div><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_register.png"></div>

3.  Εγγραφή στην Ελληνική ομάδα μετάφρασης του Gnome (Πηγαίνετε στην προσωπική σας σελίδα με κλικ στο όνομα σας μετά το login, “Join a team» (_Γίνετε μέλος μιας ομάδας_), Επιλογή της ελληνικής ομάδας, “Join” (_Γίνετε μέλος_)

<div style="margin:20px 105px 0 105px"><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_user_settings.png"> <img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_join.png"></div>

5.  Eγγραφή στην λίστα αλληλογραφίας [gnome-el](https://mail.gnome.org/mailman/listinfo/gnome-el-list)
6.  Eγκατάσταση και ρύθμιση εργαλείου μετάφρασης [προαιρετικά, οι μεταφράσεις μπορούν να γίνουν και με απλό επεξεργαστή κειμένου]

* * *

<a name="translate">

## Βήματα μετάφρασης

</a>

<a name="translate"></a>

Πριν επιλέξετε κάποιο αρχείο για μετάφραση,θα ήταν χρήσιμο να κάνετε εγκατάσταση κάποιο μεταφραστικό εργαλείο όπως επίσης να διαβάσετε και τις συμβουλές μετάφρασης.

<div style="margin-top:35px;text-align:center">

<div class="col-sm-2"><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_tips.png">Συμβουλές μετάφρασης](https://wiki.gnome.org/el/Translation/Tips)</div>

<div class="col-sm-2"><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_tools.png">Εργαλεία για μετάφραση](https://wiki.gnome.org/el/Translation/Tools)</div>

<div class="col-sm-2"><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_structure.png">Μορφή αρχείων μεταφράσης](https://wiki.gnome.org/el/Translation/Files)</div>

<div class="col-sm-2"><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translate_images.png">Μετάφραση εικόνων">(https://wiki.gnome.org/el/Translation/Screenshots)</div>

<div class="col-sm-2"><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_list.png"> Λίστα φράσεων & όρων (γλωσσάρι)](http://gnome.gr/glossary/)</div>

<div class="col-sm-2"><img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_memory.png">Μεταφραστική μνήμη](https://wiki.gnome.org/el/Translation/Memory)</div>

</div>

* * *

### Σε κάθε μετάφραση

Παρακάτω υπάρχει μια γρήγορη επισκόπηση των βημάτων που πρέπει να ακολουθήσουμε όταν μεταφράζουμε ένα πρόγραμμα.

1.  <a href="http://l10n.gnome.org/languages/el">Eπιλογή έκδοσης για μετάφραση</a>
2.  Λήψη πακέτου μετάφρασης

<img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_download_file.png">

4.  Ανακοίνωση έναρξης μετάφρασης του πακέτου. Συνδεθειτε στη σελίδα από την οποία κατεβάσατε το πακέτο, επιλέξτε Action → Reserve for translation → Submit (_Ενέργεια → Ανάληψη για μετάφραση → Υποβολή_).

<img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_reserve.png">

6.  Eφαρμογή <a href="https://wiki.gnome.org/el/Translation/Memory">μεταφραστικής μνήμης</a> [προαιρετικά, βοηθάει στα εντελώς αμετάφραστα πακέτα]
7.  Mετάφραση του πακέτου με τη χρήση του <a href="http://gnome.gr/glossary/">γλωσσαρίου</a> και της <a href="http://memory.gnome.gr">διαδικτυακής μεταφραστικής μνήμης (Pylyglot)</a>
8.  Αποστολή του αρχείου. Επιλέξτε το πακέτο από τη λίστα της σελίδας [Στατιστικών μετάφρασης του GNOME](http://l10n.gnome.org/languages/el), και μετά επιλέξτε Action → Upload the new translation (_Ενέργεια → Ανέβασμα της μετάφρασης_), αφήστε ένα σχόλιο, επιλέξτε στο File το αρχείο po που μεταφράσατε και πατήστε Submit (_Υποβολή_) [απαιτείται login].

<img src="https://static.gnome.org/gnome-gr/uploads/sites/7/2014/04/translation_upload_file.png">

10.  Η μετάφραση σας θα ελεγχθεί από έναν από τους ελεγκτές της ομάδας για τυχόν λάθη και θα καταχωρηθεί στο σύστημα.
11.  Θα σας απαντήσουν σε σύντομο χρονικό διάστημα. Στην σπάνια περίπτωση που δεν έχετε απάντηση σε διάστημα μιας εβδομάδας, στείλτε ξανά μια ερώτηση στην λίστα.

<div class="alert alert-info" style="margin-top:15px">

Αν έχετε απορίες σχετικά με τη διαδικασία της μετάφρασης, επικοινωνήστε μαζί μας στη <a href="https://mail.gnome.org/mailman/listinfo/gnome-el-list">λίστα αλληλογραφίας</a>.

</div>

* * *

<a name="bugs"></a>

<a name="bugs">

## Aναφορά σφαλμάτων

</a>

<a name="bugs"></a>

Αν κάποιος δεν θέλει να ασχοληθεί με την μετάφραση του GNOME, μπορεί να μας βοηθήσει με άλλο τρόπο. Μέσα από την χρήση της επιφάνειας εργασίας, αν διαπιστώσει έστω και ένα μικρό (ή και μεγάλο!) λάθος στις ελληνικές μεταφράσεις τότε μπορεί να προχωρήσει στην αναφορά σφαλμάτος . Οι αναφορές βοηθούν στην γρήγορη διόρθωση σφαλμάτων.

Η αναφορά σφαλμάτων αφορά κάθε είδους πρόβλημα που μπορούμε να συναντήσουμε κατά την διάρκεια χρήσης του GNOME. Πολύ χρήσιμη είναι και η αναφορά μεταφραστικών σφαλμάτων. Από την στιγμή που οι μεταφραστές ενημερωθούν για κάποιο πρόβλημα που δεν γνωρίζαν πριν, έχουν την δυνατότητα να το διορθώσουν.

### Σφάλματα που μπορούν αναφερθούν σχετικά με την ελληνική μετάφραση:

*   Ορθογραφικά λάθη
*   Συντακτικά λάθη
*   Θέματα σχετικά με την απόδοση κάποιου όρου
*   Επισήμανση προγραμμάτων που πρέπει να μεταφραστούν στα ελληνικά

### Πως γίνετε αναφορά μεταφραστικού σφάλματος:

*   Σύνδεση ή δημιουργία λογαριασμού στο [http://bugzilla.gnome.org/](http://bugzilla.gnome.org/) (χρειάζεται να δωθεί μια ηλ. διεύθυνση)
*   [Αναφορά σφάλματος σε μετάφραση](https://bugzilla.gnome.org/enter_bug.cgi?product=l10n&component=Greek%20[el] "Αναφορά σφάλματος")

Σε περίπτωση που δεν λειτουργεί ο σύνδεσμος,προσθέστε χειροκίνητα τα παρακάτω πεδία.

*   Eπιλογή **“File new bug”**,
*   Επιλογή Infrastructure → l10n,
*   Επιλογή γλώσσας **Greek[el]** στο πεδίο Component,
*   Επιλογή έκδοσης GNOME,
*   Τίτλος και περιγραφή του προβλήματος

Αν δεν θέλετε να ακουλουθήσετε την παραπάνω διαδικασία,υπάρχει ακόμα η δυνατότητα αναφοράς μεταφραστικών σφαλμάτων στη λίστα αλληλογραφίας μας:

*   [Λίστα αλληλογραφίας της κοινότητας μας gnome-el](https://mail.gnome.org/mailman/listinfo/gnome-el-list) (απαιτείται εγγραφή)
