---
author: iosifidis
categories:
  - Ειδήσεις
date: 2013-04-17 10:30:27
guid: http://gnome.gr/?p=2046
id: 2046
image: /wp-content/uploads/sites/7/2013/04/FOSSCOMM2013.jpg
permalink: /2013/04/17/gnome-fosscomm-2013/
title: Συμμετοχή της κοινότητας GNOME στο συνέδριο FOSSCOMM 2013
url: /2013/04/17/gnome-fosscomm-2013/
---

[<img class="aligncenter size-full wp-image-2047 img-responsive " alt="FOSSCOMM 2013" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/04/FOSSCOMM2013.jpg" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/04/FOSSCOMM2013.jpg)

Στις 20-21 Απριλίου θα διεξαχθεί το ετήσιο συνέδριο κοινοτήτων ανοικτού λογισμικού <a href="http://hua.fosscomm.gr/" target="_blank">FOSSCOMM</a>, στο <a href="http://www.hua.gr/index.php/el/" target="_blank">Χαροκόπειο Πανεπιστήμιο</a> απο το <a href="http://www.dit.hua.gr/" target="_blank">Tμήμα Πληροφορικής και Τηλεματικής</a>.

Η Ελληνική Κοινότητα GNOME θα συμμετάσχει όπως κάθε χρόνο με δικό του booth αλλά και με ομιλίες.
  
Συγκεκριμένα το Σάββατο το πρωί θα υπάρξει στο Αμφιθέατρο ομιλία με θέμα:
  
**«Μήπως η κοινότητα GNOME τα έχει παίξει;»**.
  
Την Κυριακή το απόγευμα θα συμμετάσχουμε στο στρογγυλό τραπέζι όπου θα διεξαχθεί συζήτηση με τους εκπροσώπους κοινοτήτων.

Μπορείτε να κατεβάσετε το <a href="http://hua.fosscomm.gr/fosscomm2013.pdf" target="_blank">πλήρες πρόγραμμα της εκδήλωσης</a>.

Θα χαρούμε να τα πούμε από κοντά.

Σημαντικοί σύνδεσμοι:
  
– Επίσημη ιστοσελίδα <a href="http://hua.fosscomm.gr/" target="_blank">http://hua.fosscomm.gr/</a>
  
– Εκδήλωση στο <a href="https://www.facebook.com/Fosscomm2013" target="_blank">Facebook</a>
  
– Twitter <a href="https://twitter.com/Fosscomm2013" target="_blank">https://twitter.com/Fosscomm2013</a>
