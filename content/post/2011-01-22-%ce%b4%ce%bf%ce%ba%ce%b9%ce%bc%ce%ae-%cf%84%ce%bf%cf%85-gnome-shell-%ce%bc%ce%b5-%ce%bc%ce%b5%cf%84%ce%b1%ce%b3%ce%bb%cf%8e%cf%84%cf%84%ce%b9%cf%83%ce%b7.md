---
author: simosx
categories:
  - Εικόνες/Στιγμιότυπα
  - Παρουσίαση
date: 2011-01-22 22:03:04
guid: http://gnome.gr/?p=741
id: 741
permalink: /2011/01/22/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae-%cf%84%ce%bf%cf%85-gnome-shell-%ce%bc%ce%b5-%ce%bc%ce%b5%cf%84%ce%b1%ce%b3%ce%bb%cf%8e%cf%84%cf%84%ce%b9%cf%83%ce%b7/
title: Δοκιμή του GNOME Shell με μεταγλώττιση
url: /2011/01/22/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae-%cf%84%ce%bf%cf%85-gnome-shell-%ce%bc%ce%b5-%ce%bc%ce%b5%cf%84%ce%b1%ce%b3%ce%bb%cf%8e%cf%84%cf%84%ce%b9%cf%83%ce%b7/
---

To <a href="http://live.gnome.org/GnomeShell/" target="_blank">GNOME Shell</a> είναι η νέα διασύνδεση χρήστη που θα έχει το <a href="http://gnome3.org/" target="_blank">GNOME 3</a>.

Θα είναι έτοιμο με το GNOME 3 και το αναμένουμε σε λίγους μήνες.

Τι μπορούμε να κάνουμε τώρα για να δούμε το GNOME Shell;

Μπορούμε να μεταγλωττίσουμε το GNOME Shell και να δοκιμάσουμε την πιο πρόσφατη έκδοση. Τόσο πρόσφατη, που θα είναι η έκδοση όπως την προγραμματίζουν τώρα οι προγραμματιστές του GNOME.

Σε αυτόν τον οδηγό θα δούμε πως μπορούμε να μεταγλωττίσουμε το Gnome Shell (Gnome 3) ώστε να έχει την πιο καινούργια έκδοση! Είναι σημαντικό να πούμε ότι αυτός ο τρόπος εγκατάστασης του GNOME Shell δεν επηρεάζει το σύστημά μας διότι γίνεται όλα τα νέα αρχεία πάνε σε ένα κατάλογο.

Τα βήματα για τη μεταγλώττιση του GNOME Shell είναι

<blockquote style="margin:22px 40px;padding:3px;color:#585858;padding: 0 50px;background: transparent;border-left: 3px solid #ccc">
  <p>
    curl -O http://git.gnome.org/browse/gnome-shell/plain/tools/build/gnome-shell-build-setup.sh
  </p>
</blockquote>

όπου λαμβάνουμε ένα πρόγραμμα εντολών που ελέγχει αν έχουμε εγκατεστημένα βοηθητικά πακέτα μεταγλώττισης.

<blockquote style="margin:22px 40px;padding:3px;color:#585858;padding: 0 50px;background: transparent;border-left: 3px solid #ccc">
  <p>
    /bin/bash gnome-shell-build-setup.sh
  </p>
</blockquote>

Αυτή η εντολή εκτελεί το πρόγραμμα εντολών. Εδώ πρέπει να διαβάσουμε με προσοχή αυτά που αναφέρει. Μπορεί να ζητήσει να εγκαταστήσουμε μια σειρά από πακέτα. Και πρέπει να το κάνουμε.

<blockquote style="margin:22px 40px;padding:3px;color:#585858;padding: 0 50px;background: transparent;border-left: 3px solid #ccc">
  <p>
    sudo rm -rf /usr/lib*/*.la
  </p>
</blockquote>

Εδώ σβήνεις τα αρχεία .la. Είναι σημαντικό να το γράψουμε σωστά, με αντιγραφή και επικόλληση. Τα αρχεία αυτά δεν απαιτούνται από το σύστημα, και παρουσιάζουν πρόβλημα κατά τη μεταγλώττιση. Είναι εντάξει να σβηστούν, και δε χρειάζεται να λάβουμε αντίγραφο ασφαλείας.

<blockquote style="margin:22px 40px;padding:3px;color:#585858;padding: 0 50px;background: transparent;border-left: 3px solid #ccc">
  <p>
    jhbuild build
  </p>
</blockquote>

Αυτή είναι η πιο σημαντική εντολή, όπου κάνει τη λήψη του πηγαίου κώδικα και μεταγλωττίζει τα πακέτα για να φτιάξει το GNOME Shell. Εδώ παρατηρούμε για σφάλματα, και επαναλαμβάνουμε μέχρι να ολοκληρωθεί. Αυτό είναι το σημείο που ελέγχουμε με προσοχή για προβλήματα. Τα πακέτα είναι τόσο φρέσκα που μπορεί να έχουν προσωρινές ατέλειες (δηλαδή ο προγραμματιστής να βάλει κάτι που να μη δουλεύει, να το διαπιστώσει και να το διορθώσει μετά από δέκα λεπτά). Το ωραίο με την ιστορία αυτή είναι ότι για πρώτη φορά είμαστε τόσο κοντά στον κώδικα που γράφεται αυτή τη στιγμή. Ο,τι πιο φρέσκο. Είναι το αντίθετο με Windows που όλος ο κώδικας έχει γραφτεί πριν από πολλά χρόνια.

<blockquote style="margin:22px 40px;padding:3px;color:#585858;padding: 0 50px;background: transparent;border-left: 3px solid #ccc">
  <p>
    cd ~/gnome-shell/source/gnome-shell/src<br /> ./gnome-shell –replace
  </p>
</blockquote>

Αν όλα πάνε καλά, έχει φτιαχτεί το ~/gnome-shell/source/gnome-shell/src/, μπαίνουμε μέσα και εκτελούμε το GNOME Shell. Για να κλείσουμε πατάμε αποσύνδεση.

Πως φαίνεται το GNOME Shell που μεταγλωττίσαμε;

[<img class="size-full wp-image-744  img-responsive " title="Στιγμιότυπο του GNOME Shell όπως το μεταγλωττίσαμε" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/01/screenshot11kh.png" alt="Στιγμιότυπο του GNOME Shell όπως το μεταγλωττίσαμε" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/01/screenshot11kh.png) [<img class="size-medium wp-image-743 img-responsive " title="Στιγμιότυπο του GNOME Shell όπως το μεταγλωττίσαμε" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/01/screenshot12am-300x168.png" alt="Στιγμιότυπο του GNOME Shell όπως το μεταγλωττίσαμε" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/01/screenshot12am.png) [<img class="size-medium wp-image-742 img-responsive " title="Στιγμιότυπο του GNOME Shell όπως το μεταγλωττίσαμε" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/01/screenshot13w-300x168.png" alt="Στιγμιότυπο του GNOME Shell όπως το μεταγλωττίσαμε" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/01/screenshot13w.png) 

<div>
  <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/gr/"><img src="http://i.creativecommons.org/l/by-nc-sa/3.0/gr/88x31.png" alt="Creative Commons License" /></a><br /> Η εργασία υπάγεται στην άδεια <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/gr/">Creative Commons Αναφορά-Παρόμοια διανομή 3.0 Ελλάδα</a>
</div>

<div>
  και προέρχεται από <a href="http://forum.ubuntu-gr.org/viewtopic.php?f=9&t=16528&p=165878">τον οδηγό του μέλους clepto</a> <a href="http://forum.ubuntu-gr.org/" target="_blank">του φόρουμ</a> της <a href="http://www.ubuntu-gr.org/" target="_blank">ελληνικής κοινότητας Ubuntu-gr</a>.
</div>
