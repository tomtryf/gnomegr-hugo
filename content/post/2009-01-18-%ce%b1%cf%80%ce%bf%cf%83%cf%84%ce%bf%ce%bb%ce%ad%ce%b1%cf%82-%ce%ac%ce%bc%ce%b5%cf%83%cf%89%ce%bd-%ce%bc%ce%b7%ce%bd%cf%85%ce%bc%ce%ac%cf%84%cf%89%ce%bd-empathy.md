---
author: Μάριος Ζηντίλης
categories:
  - Ειδήσεις
date: 2009-01-18 10:03:41
guid: http://gnome.gr/?p=149
id: 149
permalink: /2009/01/18/%ce%b1%cf%80%ce%bf%cf%83%cf%84%ce%bf%ce%bb%ce%ad%ce%b1%cf%82-%ce%ac%ce%bc%ce%b5%cf%83%cf%89%ce%bd-%ce%bc%ce%b7%ce%bd%cf%85%ce%bc%ce%ac%cf%84%cf%89%ce%bd-empathy/
title: 'Παρουσίαση εφαρμογής GNOME: Αποστολέας άμεσων μηνυμάτων Empathy'
url: /2009/01/18/%ce%b1%cf%80%ce%bf%cf%83%cf%84%ce%bf%ce%bb%ce%ad%ce%b1%cf%82-%ce%ac%ce%bc%ce%b5%cf%83%cf%89%ce%bd-%ce%bc%ce%b7%ce%bd%cf%85%ce%bc%ce%ac%cf%84%cf%89%ce%bd-empathy/
---

Το [Empathy](http://live.gnome.org/Empathy) είναι μια εφαρμογή ανταλλαγής άμεσων μηνυμάτων μεταξύ χρηστών, για το περιβάλλον επιφάνειας εργασίας GNOME. Υποστηρίζει πολλαπλά πρωτόκολλα επικοινωνίας, επιτρέποντας στο χρήστη να χρησιμοποιεί μόνο μία εφαρμογή για όλα. Είναι μέρος των εφαρμογών του GNOME.
  
<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2009/01/cebbceafcf83cf84ceb1-ceb5cf80ceb1cf86cf8ecebd-cf84cebfcf85-empathy.png" alt="Λίστα επαφών του Empathy" title="Λίστα επαφών του Empathy" class="size-full wp-image-155 img-responsive " />

### Τεχνικά χαρακτηριστικά

Το Empathy συγκεντρώνει κάτω από ένα κοινό γραφικό περιβάλλον διάφορα widgets για αποστολή άμεσων μηνυμάτων. Στηρίζεται στο framework προγραμματισμού [Telepathy](http://telepathy.freedesktop.org/wiki/) και στο [Mission Control](http://mission-control.sourceforge.net/) της Nokia, και χρησιμοποιεί το γραφικό περιβάλλον του [Gossip](http://live.gnome.org/Gossip).

Προς το παρόν στην έκδοση ανάπτυξης 2.25.4 το Empathy υποστηρίζει τα εξής πρωτόκολλα:

  * Jabber,
  * Gtalk,
  * MSN,
  * IRC,
  * και όλα όσα υποστηρίζονται από το Pidgin.

Άλλα χαρακτηριστικά είναι: διαχείριση και ταυτόχρονη χρήση πολλαπλών λογαριασμών, ομαδικές συνομιλίες, καταγραφή συνομιλιών, φωνητική κλήση και βιντεοκλήση.

Η χρήση των Telepathy και Mission Control επιτρέπει τη στενή ενσωμάτωση του Empathy με τις άλλες εφαρμογές του GNOME, και αυτό είναι και το δυνατό χαρτί του, όπως επίσης και ο λόγος για τον οποίο οι προγραμματιστές του GNOME επέλεξαν να αναπτύξουν ένα δικό τους Αποστολέα άμεσων μηνυμάτων. Κάποια σημεία στα οποία φαίνεται η ενσωμάτωση είναι:

  * Αυτόματη ρύθμιση της κατάστασης του χρήστη ως «Απουσιάζω» ανάλογα με την κατάσταση του [gnome-screensaver](http://live.gnome.org/GnomeScreensaver)
  * Αυτόματη επανασύνδεση με τη χρήση του [Network Manager](http://live.gnome.org/NetworkManager)
  * Σύνδεση των επαφών του Empathy με τις επαφές του [Evolution](http://live.gnome.org/Evolution)

Αυτές οι δυνατότητες ενσωμάτωσης και αλληλεπίδρασης του Empathy με τις υπόλοιπες εφαρμογές GNOME είναι που δίνουν ιδιαίτερη βαρύτητα στην επερχόμενη έκδοση 2.26. Ίσως χρειαστούν μερικοί μήνες και μία – δύο εκδόσεις ακόμα για να ωριμάσει η εφαρμογή, όμως όταν φτάσει σε ένα επίπεδο που θα μπορεί να περιληφθεί σε διανομές ως προεπιλεγμένη εφαρμογή, τότε είναι που θα φανεί η ισχύς της. Αρκεί να φανταστεί κανείς τους αμέτρητους τρόπους που μπορεί μια εφαρμογή γραπτής, φωνητικής και βίντεο επικοινωνίας να ενσωματωθεί με το υπόλοιπο σύστημα.

### Εγκατάσταση του Empathy

Η τρέχουσα σταθερή έκδοση της εφαρμογής (2.24.1) μπορεί να εγκατασταθεί από το Synaptic, ή από το τερματικό με την εντολή:

`sudo apt-get install empathy`

Η έκδοση αυτή 2.24.1 δεν έχει ακόμα ενσωματώσει μερικά από τα χαρακτηριστικά που αναφέρθηκαν πιο πάνω, όπως το πρωτόκολλο IRC και οι φωνητικές κλήσεις και βιντεοκλήσεις. Για να απολαύσετε τα επιπλέον αυτά χαρακτηριστικά, μπορείτε να εγκαταστήσετε το Empathy στην τελευταία έκδοση ανάπτυξης (2.25.4) [κατεβάζοντας τον κώδικα](http://ftp.acc.umu.se/pub/GNOME/sources/empathy/2.25/empathy-2.25.4.tar.bz2) και περνώντας την περιπέτεια της εγκατάστασης από πηγαίο κώδικα. Εναλλακτικά μπορείτε να περιμένετε ως το Μάρτιο που θα βγει η τελική έκδοση μαζί το GNOME 2.26.

### Συμπεράσματα

Το Empathy συμπεριλήφθηκε στο GNOME στην έκδοση 2.24. Είναι μια εφαρμογή στην οποία μέχρι στιγμής έχει δοθεί περισσότερη σημασία στη δημιουργία ενός στιβαρού πλαισίου προγραμματισμού της ίδια της εφαρμογής, παρά στην χρηστικότητά της ή τη φιλικότητα προς το χρήστη. Χαρακτηριστικό αυτού είναι η έλλειψη καλογραμμένης τεκμηρίωσης.

Αυτός είναι και ο λόγος για τον οποίο αρκετές διανομές που επιλέγουν το GNOME ως την προεπιλεγμένη επιφάνεια εργασίας τους, δεν επέλεξαν να περιλάβουν το Empathy ως τον προεγκατεστημένο αποστολέα άμεσων μηνυμάτων, αλλά αντί αυτού πρόσφεραν το πιο ώριμο Pidgin.

Οι προγραμματιστές του Empathy ανταποκρίνονται γρήγορα στις υποβολές σφαλμάτων και προτάσεων από τους χρήστες. Aυτό, σε συνδυασμό με κάποια ιδιαίτερα χαρακτηριστικά στην ανάπτυξη της εφαρμογής, αλλά και με την γενικότερη ποιότητα των εφαρμογών του GNOME, μπορεί να μας κάνει να περιμένουμε πολλά στο προσεχές μέλλον από το Empathy.

Με την κυκλοφορία της νέας έκδοσης μαζί με το GNOME 2.26 το Μάρτιο, πολλά νέα χαρακτηριστικά θα είναι διαθέσιμα για τους χρήστες, κάνοντας το Empathy να ανταγωνίζεται στα ίσα τα ανταγωνιστικά σ” αυτό προγράμματα για περίληψη στις διανομές ως προεπιλεγμένη εφαρμογή αποστολής άμεσων μηνυμάτων.

### Σύνδεσμοι

  * [To Empathy στο GNOME](http://live.gnome.org/Empathy)
  * [Άρθρο στο Ubuntu wiki](https://wiki.ubuntu.com/EmpathyVsPidginUsability) που συγκρίνει το Empathy με το Pidgin, με στόχο την περίληψη ή όχι στην έκδοση 8.10
  * [Μια ανακοίνωση](http://mail.gnome.org/archives/devel-announce-list/2008-August/msg00001.html) στη λίστα devel του GNOME για την περίληψη του Empathy, και [ένα ακόμα μήνυμα](http://www.mail-archive.com/desktop-devel-list@gnome.org/msg10696.html) που συνηγορεί υπέρ.