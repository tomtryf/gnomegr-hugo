---
author: iosifidis
blogger_author:
  - eliasps
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/3811099720917991955
blogger_permalink:
  - /2013/10/sub-teams.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-10-20 21:12:00
guid: http://gnome.gr/2013/10/20/%ce%b1%ce%bd%ce%b1%ce%ba%ce%bf%ce%af%ce%bd%cf%89%cf%83%ce%b7-%ce%b3%ce%b9%ce%b1-%cf%85%cf%80%ce%bf-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b5%cf%82-sub-teams/
id: 2908
image: /wp-content/uploads/sites/7/2013/10/qa64.png
permalink: /2013/10/20/%ce%b1%ce%bd%ce%b1%ce%ba%ce%bf%ce%af%ce%bd%cf%89%cf%83%ce%b7-%ce%b3%ce%b9%ce%b1-%cf%85%cf%80%ce%bf-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b5%cf%82-sub-teams/
title: Ανακοίνωση για υπο-ομάδες (Sub-Teams)
url: /2013/10/20/%ce%b1%ce%bd%ce%b1%ce%ba%ce%bf%ce%af%ce%bd%cf%89%cf%83%ce%b7-%ce%b3%ce%b9%ce%b1-%cf%85%cf%80%ce%bf-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b5%cf%82-sub-teams/
---

<div dir="ltr" style="text-align: left">
  Μετά τη μεγάλη επιτυχία και τη δουλειά της ομάδας του Ubuntu GNOME <a href="http://gnome.gr/2013/10/17/%cf%84%ce%bf-ubuntu-gnome-13-10-saucy-salamander-%ce%b5%ce%af%ce%bd%ce%b1%ce%b9-%ce%b5%ce%b4%cf%8e/">στην κυκλοφορία του Ubuntu GNOME 13.10</a> και ως πρώτο βήμα στην ανάπτυξη του 14.04, η ομάδα περνάει στο δεύτερο στάδιο του σχεδίου για την συμμετοχή εθελοντών που ξεκίνησε τον Σεπτέμβριο.<br />Το πρώτο στάδιο είχε μεγάλη επιτυχία διότι και τα αποτελέσματα ήταν εκπληκτικά. Σε σύντομο χρονικό διάστημα η ομάδα κατάφερε να φέρει 20 νέους δοκιμαστές και ως αποτέλεσμα τώρα υπάρχει ξεχωριστή <a href="https://launchpad.net/~ubuntugnome-qa">ομάδα QA/Testing</a> για το Ubuntu GNOME.</p> 
  
  <p>
    Μετά από αυτή την επιτυχία, η ομάδα συνεχίζει με την έναρξη του δεύτερου σταδίου συμμετοχής νέων εθελοντών, ανακοινώνοντας την δημιουργία υπο-ομάδων.
  </p>
  
  <h2 style="text-align: center">
    <a href="https://wiki.ubuntu.com/UbuntuGNOME/SubTeams">Ubuntu GNOME Sub-Teams</a>
  </h2>
  
  <p>
    Τι είναι μία υπο-ομάδα:
  </p>
  
  <ul style="text-align: left">
    <li>
      Μία ομάδα ατόμων που θέλουν να βοηθήσουν σε <b>ένα συγκεκριμένο τομέα</b>.
    </li>
    <li>
      Χρειάζονται ενεργά μέλη: Είναι σημαντικό να είναι όλα τα άτομα που συμμετέχουν ενεργά. Δεν είναι απαραίτητο να είναι ενεργά σε καθημερινή βάση, αλλά είναι απαραίτητη μία ελάχιστη συμμετοχή. Είναι προτιμότερο να αφήσει κάποιος την ομάδα προσωρινά αν δεν έχει χρόνο, και να επιστρέψει όταν μπορεί
    </li>
    <li>
      Μπορούν να συμμετέχουν και άτομα εκτός των ομάδων: φυσικά, όσοι θέλουν μπορούν να βοηθήσουν χωρίς να είναι μέλη μίας ομάδας. Για παράδειγμα, μπορεί κάποιος να συμμετέχει στην ανάπτυξη κάποιων σελίδων στο Wiki, χωρίς να είναι μέλος της ομάδας Wiki. Aλλά προτείνεται κάποιος να γίνει μέλος μίας συγκεκριμένης ομάδας, ανάλογα με το αντικείμενο, για να υπάρχει οργανωμένη προσέγγιση και συντονισμός με τα άλλα μέλη.
    </li>
    <li>
      Υπάρχει ελευθερία για αυτό-οργάνωση: κάθε υπο-ομάδα θα είναι υπεύθυνη για την οργάνωσή της ως ομάδα με τη βοήθεια των μελών της, σύμφωνα με τις ανάγκες της, τις εργασίες κλπ. Εξάλλου, κάθε ομάδα θα δουλεύει για την γενικότερη βελτίωση του Ubuntu GNOME.
    </li>
    <li>
      Τα μέλη θα πρέπει να κρατάνε τους συμμετέχοντες ενημερωμένους για κάθε σημαντική αλλαγή στην υπο-ομάδα με τη<a href="https://lists.ubuntu.com/mailman/listinfo/ubuntu-gnome"> χρήση της γενικής λίστας ηλεκτρονικού ταχυδρομίου</a> ή των λιστών της κάθε υπο-ομάδας.
    </li>
  </ul>
  
  <div>
  </div>
  
  <h2 style="text-align: center">
    Οι υπο-ομάδες του Ubuntu GNOME
  </h2>
  
  <div>
  </div>
  
  <h3 style="text-align: center">
    <span style="font-weight: normal">Ομάδα QA/Testing (δοκιμών)</span>
  </h3>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/qa64.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/qa64.png"  class="img-responsive" /></a>
  </div>
  
  <div>
    <span style="font-weight: normal"><br /></span>
  </div>
  
  <div>
    <span style="font-weight: normal">Βρήκατε κάποιο bug στο Ubuntu GNOME και θέλετε να διορθωθεί; Κάνατε κάποια παραμετροποίηση και συναντήσατε προβλήματα; Δεν μπορείτε να περιμένετε 6 μήνες για να εγκαταστήσετε τη νέα έκδοση; Τότε βοηθήστε την&nbsp;<a href="https://launchpad.net/~ubuntugnome-qa">ομάδα των δοκιμαστών (Ubuntu GNOME QA Team)</a>. Παρακαλούμε διαβάστε <a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing">εδώ</a> περισσότερες πληροφορίες για τις δοκιμές.</span>
  </div>
  
  <div>
    <ul style="text-align: left">
      <li>
        <a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing">Σελίδα της Ubuntu GNOME QA Team στο Wiki</a>
      </li>
      <li>
        <a href="https://launchpad.net/~ubuntugnome-qa">Σελίδα της Ubuntu GNOME QA Team στο Launchpad</a>
      </li>
    </ul>
    
    <div>
    </div>
    
    <h3 style="text-align: center">
      <span style="font-weight: normal">Oμάδα Τεκμηρίωσης και Wiki</span>
    </h3>
  </div>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/wiki64.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/wiki64.png"  class="img-responsive" /></a>
  </div>
  
  <div>
    <span style="font-weight: normal"><br /></span>
  </div>
  
  <div>
    Θέλετε να βοηθήσετε στην τεκμηρίωση και στην συντήρηση των σελίδων στο Wiki; Τότε βοηθήστε την <a href="https://launchpad.net/~ubuntugnome-wiki-docs">ομάδα Τεκμηρίωσης και Wiki (Ubuntu GNOME Wiki and Documentation Team)</a>.
  </div>
  
  <div>
    <ul style="text-align: left">
      <li>
        <a href="https://wiki.ubuntu.com/UbuntuGNOME/DocumentationTeam">Σελίδα της Ubuntu GNOME Wiki and Documentation Team στο Wiki</a>
      </li>
      <li>
        <a href="https://launchpad.net/~ubuntugnome-wiki-docs">Σελίδα της Ubuntu GNOME Wiki and Documentation Team στο Launchpad</a>
      </li>
    </ul>
    
    <div>
    </div>
    
    <h3 style="text-align: center">
      <span style="font-weight: normal">Oμάδα Προώθησης και Επικοινωνιών</span>
    </h3>
  </div>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/people48.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/people48.png"  class="img-responsive" /></a>
  </div>
  
  <div>
    <span style="font-weight: normal"><br /></span>
  </div>
  
  <div>
    <span style="font-weight: normal">Είστε ήδη ευχαριστημένοι χρήστες του Ubuntu GNOME και θέλετε να το μοιραστείτε; Θέλετε να προωθήσετε το Ubuntu GNOME; Γνωρίζετε από Facebook, Google+ και Twitter; Είστε υποστηρικτές του Linux; Γίνετε μέλη της ομάδας προώθησης και επικοινωνιών (Ubuntu GNOME Communications Team).</span>
  </div>
  
  <div style="text-align: left">
    <ul style="text-align: left">
      <li>
        <a href="https://wiki.ubuntu.com/UbuntuGNOME/CommunicationsTeam">Σελίδα της Ubuntu GNOME&nbsp;Communications&nbsp;Team στο Wiki</a>
      </li>
      <li>
        <a href="https://launchpad.net/~ubuntugnome-comms">Σελίδα της Ubuntu GNOME&nbsp;Communications&nbsp;Team στο Launchpad</a>
      </li>
    </ul>
    
    <div>
    </div>
  </div>
  
  <h3 style="text-align: center">
    <span style="font-weight: normal">Ομάδα Artwork και Σχεδίασης</span>
  </h3>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/artwork64.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/artwork64.png"  class="img-responsive" /></a>
  </div>
  
  <div>
    <span style="font-weight: normal"><br /></span>
  </div>
  
  <div>
    Σας αρέσει η σχεδίαση στο Ubuntu GNOME και θέλετε να βοηθήσετε; Διαβάστε τις πληροφορίες στη <a href="https://wiki.ubuntu.com/UbuntuGNOME/Artwork">σχετική σελίδα</a> και γίνετε μέλος στην <a href="https://launchpad.net/~ubuntugnome-artwork">ομάδα Artwork και σχεδίασης (Ubuntu GNOME Artwork Team)</a>.
  </div>
  
  <div>
    <ul style="text-align: left">
      <li>
        <a href="https://wiki.ubuntu.com/UbuntuGNOME/Artwork">Σελίδα της&nbsp;Ubuntu GNOME Artwork Team στο Wiki</a>
      </li>
      <li>
        <a href="https://launchpad.net/~ubuntugnome-artwork">Σελίδα της&nbsp;Ubuntu GNOME Artwork Team στο Launchpad</a>
      </li>
    </ul>
  </div>
  
  <div>
    <span style="font-weight: normal"><br /></span>
  </div>
  
  <h3 style="text-align: center">
    <span style="font-weight: normal">Ομάδα Ανάπτυξης</span>
  </h3>
  
  <div style="clear: both;text-align: center">
    <a href="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/ubuntugnome64.png" style="margin-left: 1em;margin-right: 1em"><img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/ubuntugnome64.png"  class="img-responsive" /></a>
  </div>
  
  <div>
    <span style="font-weight: normal"><br /></span>
  </div>
  
  <div>
    Θέλετε να δημιουργήσετε εφαρμογές και κώδικα για το Ubuntu GNOME; Αφού διαβάσετε τις <a href="https://wiki.ubuntu.com/UbuntuGNOME/Developers">πληροφορίες</a> που χρειάζονται, γίνετε μέλος στην ομάδα Ανάπτυξης (Ubuntu GNOME Developers).
  </div>
  
  <div>
    <ul style="text-align: left">
      <li>
        <a href="https://wiki.ubuntu.com/UbuntuGNOME/Developers">Σελίδα της&nbsp;Ubuntu GNOME Developers Team στο Wiki</a>
      </li>
      <li>
        <a href="https://launchpad.net/~ubuntu-gnome-dev">Σελίδα της&nbsp;Ubuntu GNOME Developers Team στο Launchpad</a>
      </li>
    </ul>
    
    <div>
    </div>
  </div>
  
  <h2 style="text-align: center">
    &nbsp;Η ομάδα Ubuntu GNOME ζητάει εθελοντές
  </h2>
  
  <div>
    Όπως είδαμε σε <a href="http://gnome.gr/2013/09/22/%ce%b6%ce%b7%cf%84%ce%bf%cf%8d%ce%bd%cf%84%ce%b1%ce%b9-%ce%b5%ce%b8%ce%b5%ce%bb%ce%bf%ce%bd%cf%84%ce%ad%cf%82-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b1-ubuntu-gnome/">παλαιότερες ανακοινώσεις</a>, καθώς και σε αυτό το <a href="http://www.youtube.com/watch?v=kG2Vum24iQc">video</a> για το <a href="http://gnome.gr/2013/09/27/%ce%b4%ce%b5%ce%af%cf%84%ce%b5-%cf%80%cf%89%cf%82-%ce%bc%cf%80%ce%bf%cf%81%ce%b5%ce%af%cf%84%ce%b5-%ce%bd%ce%b1-%cf%83%cf%85%ce%bd%ce%b5%ce%b9%cf%83%cf%86%ce%ad%cf%81%ce%b5%cf%84%ce%b5-%cf%83%cf%84/">πως να συνεισφέρετε στο Ubuntu GNOME</a>, η ομάδα συνεχίζει να ζητάει την βοήθεια, την υποστήριξη και την συνεισφορά σας. Όπως πάντα, το ζητούμενο είναι η ποιότητα και όχι η ποσότητα. Η ομάδα έχει όραμα και είναι σίγουρη πως με τη βοήθεια σας και την διάθεση για περισσότερη δουλειά, &nbsp;μπορούμε όλοι μαζί να κάνουμε περισσότερα και το Ubuntu GNOME μπορεί να γίνει μία από τις καλύτερες διανομές Linux που υπάρχουν. Γιατί όχι; Δεν μπορεί τίποτα να μας κάνει να σταματήσουμε να ονειρευόμαστε και φανταζόμαστε, και όπως είπε ο Albert Einstein:
  </div>
  
  <blockquote>
    <p>
      <i>«Η φαντασία είναι πιο σημαντική από την γνώση. Γιατί η γνώση περιορίζεται σε αυτά που ξέρουμε τώρα και καταλαβαίνουμε, ενώ η φαντασία αγκαλιάζει ολόκληρο τον κόσμο και τα πάντα που μπορούμε να μάθουμε και να καταλάβουμε.»</i>
    </p>
  </blockquote>
  
  <p>
    Πιστεύουμε πως είναι δυνατό και μπορούμε να το κάνουμε.
  </p>
  
  <p>
    Όπως πάντα, ευχαριστούμε που επιλέξατε το Ubuntu GNOME και ανυπομονούμε να συνεργαστούμε και να περάσουμε καλά.
  </p>
</div>
