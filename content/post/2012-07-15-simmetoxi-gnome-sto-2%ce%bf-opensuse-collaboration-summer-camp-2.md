---
author: iosifidis
categories:
  - Ειδήσεις
  - Παρουσίαση
date: 2012-07-15 12:39:39
guid: http://gnome.gr/?p=1663
id: 1663
permalink: /2012/07/15/simmetoxi-gnome-sto-2%ce%bf-opensuse-collaboration-summer-camp-2/
title: Συμμετοχή κοινότητας Gnome στο 2ο openSUSE collaboration summer camp
url: /2012/07/15/simmetoxi-gnome-sto-2%ce%bf-opensuse-collaboration-summer-camp-2/
---

<a href="http://gnome.gr/?attachment_id=1658" rel="attachment wp-att-1658"><img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/07/banner_big.png" alt="" title="2ο openSUSE collaboration summer camp" class="aligncenter size-full wp-image-1658 img-responsive " /></a>

Η Ελληνική Κοινότητα openSUSE ξεκίνησε πέρσι την διοργάνωση της εκδήλωσης collaboration summer camp με σκοπό να έρθουν πιο κοντά οι κοινότητες, ενθαρρύνοντας τη συνεργασία και την εργασία όλων πάνω στα projects. Αυτό καθίστανται πιο εύκολο διότι το καλοκαίρι όλος ο κόσμος χαλαρώνει και ξεκουράζεται. Παράλληλα με το έργο που παράγεται, το πακέτο περιλαμβάνει και διασκέδαση. Παιχνίδια στην πισίνα, πόλεμο με νεροπίστολα, παιχνίδια με μπάλες, ηλιοθεραπεία, φαγητό και μπύρες.

Πέρσι λοιπόν, στην επιτυχημένη διοργάνωση, συμμετείχε και η Ελληνική Κοινότητα Gnome.
  
Φέτος η διοργάνωση θα διεξαχθεί στις 20-22 Ιουλίου, στο ίδιο μέρος (Κατερίνη). Η Ελληνική Κοινότητα Gnome θα βρίσκεται εκεί με ένα workshop. Αυτό που έχουμε οργανώσει, σε συνεργασία με τους συντονιστές των μεταφράσεων, είναι η μετάφραση ορισμένων αρχείων ώστε να ανέβει το ποσοστό των μεταφράσεων. Το workshop θα πραγματοποιηθεί την Κυριακή (αναμένετε το πρόγραμμα). Μπορείτε να συμμετέχετε και όσοι θέλετε είτε απομακρυσμένα (μέσω IRC στο κανάλι gnome-el στο freenode), είτε αν μένετε κοντά στην Κατερίνη, μπορείτε να πάρετε laptop, μαγιό και διάθεση και να έρθετε μαζί μας. Θα είστε ευπρόσδεκτοι.

Φέτος έχουν δηλώσει μέχρι στιγμής συμμετοχή οι κοινότητες Fedora, Gentoo, Gnome, KDE, Mozilla, Ubuntu. Θα υπάρχει και συμμετοχή από το εξωτερικό όπου θα αναλυθεί η τεχνολογία ARM.
  
Για συμμετοχές, μπορείτε να ανατρέξετε στην [ιστοσελίδα](http://www.os-el.gr/content/%CF%83%CF%85%CE%BC%CE%BC%CE%B5%CF%84%CE%BF%CF%87%CE%AE-%CF%83%CF%84%CE%BF-2%CE%BF-opensuse-collaboration-summer-camp) της κοινότητας openSUSE.

**Θα σας περιμένουμε στην Κατερίνη!!!**

<a href="http://gnome.gr/?attachment_id=1665" rel="attachment wp-att-1665"><img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2012/07/summer_camp-300x225.jpg" alt="" title="Φωτογραφία από το 1ο openSUSE Collaboration Summer Camp" class="aligncenter size-medium wp-image-1665 img-responsive " /></a>