---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2011-03-08 23:41:00
guid: http://gnome.gr/?p=756
id: 756
permalink: /2011/03/08/%cf%80%cf%81%ce%bf%cf%82-%cf%84%ce%bf-gnome-3-0/
title: Προς το GNOME 3.0
url: /2011/03/08/%cf%80%cf%81%ce%bf%cf%82-%cf%84%ce%bf-gnome-3-0/
---

Αυτή τη στιγμή προχωράει η ανάπτυξη του <a title="GNOME 3.0" href="http://gnome3.org/" target="_blank">GNOME 3.0</a> και αναμένουμε την νέα έκδοση μέσα στον Απρίλιο 2011.

Μπορείτε να δοκιμάσετε και τώρα <a title="Δοκιμάστε το GNOME 3.0" href="http://gnome3.org/tryit.html" target="_blank">το GNOME 3.0 (προέκδοση)</a> με χρήση ειδικού LiveCD/LiveUSB, δίχως πολύ κόπο.

Αν θέλετε όπως να πάτε ένα βήματα παραπέρα, μπορείτε να μεταγλωττίσετε οι ίδιοι το GNOME 3 (GNOME Shell) στη διανομή που έχετε τώρα, και από εκεί να το δοκιμάσετε, δίχως κίνδυνο με τα δεδομένα σας. Έχουμε περιγράψει τη μέθοδο ήδη στο άρθρο <a title="Δοκιμάστε το GNOME 3 με μεταγλώττιση" href="http://gnome.gr/2011/01/22/%CE%B4%CE%BF%CE%BA%CE%B9%CE%BC%CE%AE-%CF%84%CE%BF%CF%85-gnome-shell-%CE%BC%CE%B5-%CE%BC%CE%B5%CF%84%CE%B1%CE%B3%CE%BB%CF%8E%CF%84%CF%84%CE%B9%CF%83%CE%B7/" target="_blank">Δοκιμάστε το GNOME 3 με μεταγλώττιση</a>.

Στην ελληνική κοινότητα Ubuntu <a title="Εγκατάσταση - Μεταγλώττιση του Gnome Shell" href="http://forum.ubuntu-gr.org/viewtopic.php?f=9&t=16528" target="_blank">δοκιμάσαμε και καταφέραμε να μεταγλωττίσουμε το GNOME 3</a>. Το νήμα της συζήτησης ξεπέρασε τα 170 μηνύματα, και μπορείτε και εσείς δοκιμάσετε.

[<img class="alignnone size-medium wp-image-758 img-responsive " title="Περιβάλλον GNOME 3 (προέκδοση)" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/03/screenshot6dk-300x225.png" alt="δείχνει την προέκδοση του GNOME 3, και την επιλογή Παράθυρο" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/03/screenshot6dk.png)

Στο GNOME Shell έχουμε δύο βασικές προβολές, προβολή των παραθύρων όπου βλέπουμε τις ανοιχτές εφαρμογές σε μικρογραφίες, και την προβολή των εφαρμογών όπου μπορούμε να ξεκινήσουμε τις εγκαταστημένες εφαρμογές. Παραπάνω βλέπουμε τις μικρογραφίες των εφαρμογών που τυχαίνει να τρέχουμε αυτή τη στιγμή. Βλέπουμε ένα τερματικό, το παράθυρο του διαχειριστή παραθύρων (Nautilus), το Shotwell (διαχείριση φωτογραφιών) και ένα παράθυρο του Firefox.

Στα αριστερά έχουμε συντομεύσεις εφαρμογών και βλέπουμε ότι έχουμε καθορίσει έξη τέτοιες συντομεύσεις.

[<img class="alignnone size-medium wp-image-757 img-responsive " title="Περιβάλλον GNOME Shell (προέκδοση του GNOME 3)" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/03/screenshot5tb-300x225.png" alt="δείχνει τις διαθέσιμες εφαρμογές μέσα από το GNOME Shell" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/03/screenshot5tb.png)

Εδώ βλέπουμε τις διαθέσιμες εφαρμογές του συστήματός μας, μέσα από το GNOME Shell. Αντί να βλέπουμε τις εφαρμογές μέσα από μενού, τις βλέπουμε σε μια λίστα. Στα δεξιά μπορούμε να φιλτράρουμε τη λίστα (για παράδειγμα, μπορούμε να δούμε εφαρμογές που έχουν να κάνουν με Sound & Video).

Ακόμα, έχουμε πατήσει το κουμπί της Προσιτότητας (Accessibility) από την επάνω μπάρα και βλέπουμε τις επιλογές προσιτότητας που παρέχει το GNOME Shell.

<a title="Σελίδα στατιστικών της μετάφρασης του GNOME 3" href="http://l10n.gnome.org/teams/el" target="_blank">Η μετάφραση του GNOME 3 συνεχίζεται</a>, και προχωρούμε ώστε να έχουμε ένα πλήρες μεταφρασμένο περιβάλλον. Μπορείτε να γραφτείτε <a title="Λίστα gnome.gr" href="https://mail.gnome.org/mailman/listinfo/gnome-el-list" target="_blank">στη λίστα (mailing list) της ελληνικής κοινότητας του GNOME</a> για επικοινωνία με τα υπόλοιπα μέλη.

Η άνοιξη του 2011 θα έρθει με νέο λογισμικό για την επιφάνεια εργασίας του ελεύθερου λογισμικού. Αν η διανομή σας είναι Fedora, OpenSUSE ή Ubuntu, θα δείτε ένα νέο περιβάλλον. Αυτή η αλλαγή ξεπερνάει αυτό που είχαμε συνηθίσει εδώ και δεκαπέντε χρόνια στο χώρο της πληροφορικής, και είναι στο χέρι μας να μάθουμε κάτι καινούριο. Όπως σε κάθε καινούριο, θα υπάρξουν άτομα που θα αντιμετωπίσουν προβλήματα στη χρήση του νέου λογισμικού. Και θέλουμε, μέσα από την κοινότητα του GNOME.gr να βοηθήσουμε όσο μπορούμε.

(θα ήθελα να ευχαριστήσω <a title="χρήστης NickMrg (φόρουμ Ubuntu-gr)" href="http://forum.ubuntu-gr.org/memberlist.php?mode=viewprofile&u=6743" target="_blank">το χρήστη NickMrg</a> για τα παραπάνω στιγμιότυπα)
