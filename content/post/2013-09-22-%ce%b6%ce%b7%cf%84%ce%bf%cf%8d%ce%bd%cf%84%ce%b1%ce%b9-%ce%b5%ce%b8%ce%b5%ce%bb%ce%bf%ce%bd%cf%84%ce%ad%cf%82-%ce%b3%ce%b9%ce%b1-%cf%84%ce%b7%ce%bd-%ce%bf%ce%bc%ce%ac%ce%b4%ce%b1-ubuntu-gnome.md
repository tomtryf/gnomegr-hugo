---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/2216917131993106500
blogger_permalink:
  - /2013/09/ubuntu-gnome.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-09-22 13:38:00
guid: http://gnome.gr/2013/09/22/%ce%b6%ce%b7%cf%84%ce%bf%cf%8d%ce%bd%cf%84%ce%b1%ce%b9-%ce%b5%ce%b8%ce%b5%ce%bb%ce%bf%ce%bd%cf%84%ce%ad%cf%82-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b1-ubuntu-gnome/
id: 2912
image: /wp-content/uploads/sites/7/2013/09/logo_medium.png
permalink: /2013/09/22/%ce%b6%ce%b7%cf%84%ce%bf%cf%8d%ce%bd%cf%84%ce%b1%ce%b9-%ce%b5%ce%b8%ce%b5%ce%bb%ce%bf%ce%bd%cf%84%ce%ad%cf%82-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b1-ubuntu-gnome/
title: Ζητούνται εθελοντές για την ομάδα Ubuntu GNOME
url: /2013/09/22/%ce%b6%ce%b7%cf%84%ce%bf%cf%8d%ce%bd%cf%84%ce%b1%ce%b9-%ce%b5%ce%b8%ce%b5%ce%bb%ce%bf%ce%bd%cf%84%ce%ad%cf%82-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b7%ce%bd-%ce%bf%ce%bc%ce%ac%ce%b4%ce%b1-ubuntu-gnome/
---

<img border="0" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/09/logo_medium-300x35.png"  class="img-responsive" />

Όσοι παρακολουθείτε την παγκόσμια κοινότητα Ubuntu, έχει κάνει δυναμική την παρουσία της η νεοσύστατη ομάδα Ubuntu GNOME, αν και ακόμα έχει λίγα δραστήρια άτομα. Έχει στηθεί ιστοσελίδα, wiki με όλες τις πληροφορίες, social media, κανάλι IRC. Εκτός από αυτά, δουλεύουν πολλοί προγραμματιστές αλλά και δοκιμαστές, ώστε να βγει ένα όσο το δυνατό πιο ολοκληρωμένο προϊόν. Η ανταπόκριση των τελικών χρηστών είναι θετική, καθώς θεωρούν ότι το GNOME έλειψε από το Ubuntu.

Όπως έχει αναφερθεί και στη <a href="https://wiki.ubuntu.com/UbuntuGNOME/Meetings/16092013" target="1">μηνιαία συνάντηση</a> στο κανάλι, έχει ξεκινήσει μια παγκόσμια προσπάθεια για να βρεθούν άτομα κλειδιά που θα βοηθήσουν σε διάφορους τομείς στην ομάδα GNOME. Η προσπάθεια αυτή θα ολοκληρωθεί με την ημέρα κυκλοφορίας της Ubuntu 13.10 και θα συνεχιστεί μόνο εάν υπάρχει ανάγκη.

Για να βελτιωθούν λοιπόν όλα που αφορούν το Ubuntu GNOME, χρειαζόμαστε να μεγαλώσουμε ως ομάδα. Αυτό δεν σημαίνει απαραίτητα ότι χρειαζόμαστε περισσότερα μέλη. Χρειαζόμαστε ΠΟΙΟΤΗΤΑ και όχι ΠΟΣΟΤΗΤΑ. Είναι λιγότερο εποικοδομητικό εάν υπάρχουν 50 άτομα στην ομάδα και κανένα από αυτά δεν είναι διαθέσιμο όταν χρειαστεί.

**ΤΟΜΕΙΣ ΠΟΥ ΜΠΟΡΕΙΤΕ ΝΑ ΒΟΗΘΗΣΕΤΕ**

  * **Ανάπτυξη:** Υπάρχουν καταπληκτικοί και ταλαντούχοι προγραμματιστές και θέλουμε περισσότερες επιλογές.
  * **Δοκιμαστές:** Χρειαζόμαστε ΠΕΡΙΣΣΟΤΕΡΟΥΣ. Είναι πολύ σημαντικό. Βρείτε όσα περισσότερα σφάλματα μπορείτε και κάντε αναφορά στην λίστα ώστε να προωθηθούν προς διόρθωση.
  * **Τεκμηρίωση και WiKi:** Είναι σημαντικό να υπάρχει τεκμηρίωση ακόμα και για το πιο τέλειο προϊόν. Είναι κάτι που οι προγραμματιστές δεν είναι καλοί να κάνουν. Χρειάζονται άτομα και για το Αγγλικό αλλά και το Ελληνικό κομμάτι (επικοινωνήστε μαζί μας).
  * **Επικοινωνία και Marketing [Website, Facebook, Twitter και Google+]:** Μέχρι τώρα τα διαχειρίζεται ένα άτομο. Όμως χρειάζεται βοήθεια. Όσο πιο πολλοί, τόσο καλύτερα. Δεν έχουμε ξεκινήσει ακόμα κάτι για την Ελληνική Ιστοσελίδα. Όμως αν κάποιος ενδιαφέρεται να βοηθήσει, μπορούμε να το ξεκινήσουμε. Μην ξεχνάτε ότι η Ελληνική προσπάθεια μπορεί να υποστηριχθεί από την Ελληνική κοινότητα Ubuntu οπότε, μπορείτε να βοηθήσετε και εκεί.
  * **Artwork:** Διαθέτουμε έναν πολύ έξυπνο και δραστήριο άτομο που συντονίζει την ομάδα Artwork αλλά χρειάζεται βοήθεια ώστε να υπάρχουν περισσότερες επιλογές. Κάποιος πχ αναλαμβάνει το Slide Show, άλλος το Branding, άλλος τα wallpapers κλπ.

Από τις παραπάνω κατηγορίες, μερικές είναι με μεγαλύτερη προτεραιότητα (πχ πρέπει να βρεθούν περισσότεροι δοκιμαστές παρά άτομα για την ομάδα Artwork).

**ΧΡΕΙΑΖΟΜΑΣΤΕ ΤΗΝ ΒΟΗΘΕΙΑ ΣΑΣ**

  * <a href="https://wiki.ubuntu.com/UbuntuGNOME/GettingInvolved" target="1">https://wiki.ubuntu.com/UbuntuGNOME/GettingInvolved</a>
  * Εάν ενδιαφέρεστε για οποιοδήποτε από τα παραπάνω, γράψτε τις λεπτομέρειες για εσάς εδώ: <a href="https://wiki.ubuntu.com/UbuntuGNOME/GettingInvolved/WhoWeAre" target="1">https://wiki.ubuntu.com/UbuntuGNOME/GettingInvolved/WhoWeAre</a>
  * Χρειαζόμαστε άτομα που θα μείνουν. Δεν θέλουμε κάποιους που θα βρίσκονται σε προσωρινή βάση και όποτε τους καπνίσει.
  * Όπως ξέρετε, αυτή είναι εθελοντική δουλειά. Κάνετε ότι αγαπάτε και παθιάζεστε. Δεν χρειάζεται να το κάνετε αυτό για κανένα λόγο. Εάν θέλετε να δώσετε πίσω στην κοινότητα Ubuntu GNOME και στο Linux γενικότερα, σας ευχαριστούμε. Εάν δεν μπορείτε, το καταλαβαίνουμε απόλυτα.

Εάν έχετε οποιαδήποτε ερώτηση, εδώ είμαστε για εσάς.

Σας ευχαριστούμε για την στήριξή σας.

Αρχικό μήνυμα στην λίστα ταχυδρομείου  
<a href="https://lists.ubuntu.com/archives/ubuntu-gnome/2013-September/000728.html" target="1">https://lists.ubuntu.com/archives/ubuntu-gnome/2013-September/000728.html</a>