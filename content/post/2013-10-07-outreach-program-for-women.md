---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2013-10-07 12:00:52
guid: http://gnome.gr/?p=2149
id: 2149
permalink: /2013/10/07/outreach-program-for-women/
tags:
  - OPW
  - Outreach Program for Women
  - Women
title: Outreach Program for Women
url: /2013/10/07/outreach-program-for-women/
---

Είδαμε σε προηγούμενα άρθρα, τι είναι η ομάδα <a href="http://gnome.gr/2013/10/07/gnome-women/" target="_blank">GNOME Women</a> καθώς επίσης και την <a href="http://gnome.gr/2013/10/07/gnome-women-no1/" target="_blank">1η συνάντησή της στη Θεσσαλονίκη</a>. Ένας από τους κυρίως σκοπούς της ομάδας είναι και το <a href="https://wiki.gnome.org/OutreachProgramForWomen" target="_blank">Outreach Program for Women</a> (OPW).<figure id="attachment_2161" style="width: 231px" class="wp-caption aligncenter">

[<img class="size-medium wp-image-2161 img-responsive " alt="Αφίσα Outreach Program for Women" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/opw-poster-2013-December-March-231x300.png" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/opw-poster-2013-December-March.png)<figcaption class="wp-caption-text">Αφίσα Outreach Program for Women</figcaption></figure> 

## ΤΙ ΕΙΝΑΙ ΤΟ OPW;

Το OPW αποτελεί ένα πρόγραμμα για γυναίκες που σκοπό έχει να τις ενθαρρύνει να συνεισφέρουν στο ΕΛ/ΛΑΚ και ειδικότερα στο GNOME. Αρχικά σκοπός του είναι να συμμετέχουν γυναίκες σε Internship ή ως Google Summer of Code (GSoC) students με απώτερο σκοπό να βρεθεί κάποια μόνιμη θέση εργασίας. Αυτό, πάντα, εξαρτάται από τη φιλοδοξία του κάθε ατόμου.

## ΣΕ ΤΙ ΘΕΜΑΤΑ ΜΠΟΡΕΙΤΕ ΝΑ ΔΟΥΛΕΨΕΤΕ;

  1. Ανάπτυξη λογισμικού
  2. System administration
  3. Σχεδίαση της γραφικής διεπαφής
  4. Σχεδιασμός γραφικών
  5. Τεκμηρίωση
  6. Διαχείριση κοινότητας
  7. Engagement (πρώην Marketing)
  8. Αναγνώριση σφαλμάτων και αναφορά τους στο σύστημα αναφορών σφαλμάτων
  9. Βοήθεια χρηστών
 10. Οργάνωση εκδηλώσεων
 11. Μεταφράσεις

Αν δείτε κάτι από την παραπάνω λίστα και σας ενδιαφέρει, μην αγχώνεστε εάν θα τα καταφέρετε, εάν δεν ξέρετε τι θα συναντήσετε κλπ. Θα βρείτε κάποιον mentor που θα σας βοηθήσει. Θα σας υποδείξει τι χρειάζεται να διαβάσετε, πηγές κλπ ώστε να μπορέσετε να συνεχίσετε. Ακόμα και αν αντιμετωπίσετε κάποιο πρόβλημα, μπορείτε να ρωτήσετε τον mentor ή την κοινότητα (στα κανάλια επικοινωνίας) και σίγουρα θα σας δοθεί απάντηση.

## ΕΧΕΙ DEADLINES;

_Υποποβολή αίτησης:_ **1 ΟΚΤΩΒΡΙΟΥ έως 11 ΝΟΕΜΒΡΙΟΥ**
  
_Απάντηση:_ **20 ΝΟΕΜΒΡΙΟΥ**
  
_Διάρκεια:_ **10 ΔΕΚΕΜΒΡΙΟΥ – 10 ΜΑΡΤΙΟΥ**

## ΕΧΕΙ ΚΑΙ ΠΛΗΡΩΜΕΣ

**$500** εφόσον σας δεχτούν
  
**$2250** μετά από τον πρώτο έλεγχο
  
**$2250** με το πέρας της εργασίας σας

**ΠΕΡΙΟΡΙΣΜΟΣ:** Δεν πρέπει να σας έχουν δεχτεί ΞΑΝΑ είτε στο OPW είτε στο GSoC. Επίσης πρέπει να είστε πάνω από 18 ετών.

## ΔΕΝ ΠΡΟΛΑΒΑ ΝΑ ΚΑΝΩ ΑΙΤΗΣΗ

Σίγουρα θα υπάρχει στο μέλλον. Επίσης θα υπάρξει και το GSoC (φοιτήτριες).
  
Όμως μπορείτε να δείτε τις ευκαιρίες στους οργανισμούς που συμμετέχουν και στο OPW και στο GSoC (internships or full-time): <a href="https://wiki.gnome.org/OutreachProgramForWomen/Opportunities" target="_blank">https://wiki.gnome.org/OutreachProgramForWomen/Opportunities</a>

## ΠΩΣ ΝΑ ΞΕΚΙΝΗΣΕΤΕ

Καταρχήν διαβάστε προσεκτικά τις οδηγίες-ιστοσελίδες:

1. <a href="https://wiki.gnome.org/OutreachProgramForWomen" target="_blank">https://wiki.gnome.org/OutreachProgramForWomen</a>
  
2. <a href="https://wiki.gnome.org/GnomeWomen/OutreachProgram/PlanningNotes" target="_blank">https://wiki.gnome.org/GnomeWomen/OutreachProgram/PlanningNotes</a>
  
3. <a href="https://projects.gnome.org/outreach/women/" target="_blank">https://projects.gnome.org/outreach/women/</a>

Τα βήματα που πρέπει να κάνετε είναι:

  1. Γραφτείτε στην λίστα: <a href="https://mail.gnome.org/mailman/listinfo/opw-list" target="_blank">https://mail.gnome.org/mailman/listinfo/opw-list</a>
  2. Στείλτε ένα mail και συστηθείτε. Τι γνωρίζετε, με τι ενδιαφέρεστε να ασχοληθείτε.
  3. Επικοινωνήστε μέσω του καναλιού IRC **#opw** στον **irc.gnome.org**
  4. Στα παραπάνω κανάλια επικοινωνίας, ζητήστε mentor για να σας βοηθήσει να βρείτε το ακριβές θέμα σας αλλά και να σας υποδείξει (μαζί με την κοινότητα) που να κάνετε την αίτησή σας.

[<img class="aligncenter size-medium wp-image-2152 img-responsive " alt="opw-cartoon" src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/opw-cartoon-240x300.png" />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/opw-cartoon.png)
  
Εάν σας έχει μπερδέψει η διαδικασία, δείτε το παραπάνω χαριτωμένο cartoon poster.

Επίσης μπορείτε να δείτε την ομιλία της Marina Zhurakhinskaya (υπεύθυνη για το GNOME), στο συνέδριο GUADEC 2013, με τίτλο <a href="http://www.superlectures.com/guadec2013/outreach-program-for-women-lessons-in-collaboration" target="_blank">Outreach Program for Women: Lessons in Collaboration</a>.

Μπορείτε να προωθήσετε το πρόγραμμα στις ομάδες σας, είτε ενθαρρύνοντας να διαβάσουν το παρόν άρθρο, είτε να χρησιμοποιήσετε τα παραπάνω poster.
