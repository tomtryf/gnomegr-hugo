---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2014-09-27 13:16:44
guid: http://gnome.gr/?p=2717
id: 2717
image: /wp-content/uploads/sites/7/2014/09/l10n.png
permalink: /2014/09/27/euxaristies-gia-tin-ekdosi-gnome-3-14/
tags:
  - gnome
  - gnome 3.14
  - gnome shell
  - l10n
  - ΕΛ/ΛΑΚ
  - μετάφραση
title: Ευχαριστίες για την έκδοση GNOME 3.14
url: /2014/09/27/euxaristies-gia-tin-ekdosi-gnome-3-14/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/l10n-300x172.png" alt="l10n" class="aligncenter size-medium wp-image-2734 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/l10n.png)

Εδώ και λίγο καιρό κυκλοφόρησε η [νέα έκδοση](http://gnome.gr/2014/09/24/nea-ekdosi-gnome-3-14/ "Νέα έκδοση GNOME 3.14") του GNOME 3.14 με αρκετά χαρακτηριστικά και βελτιώσεις σφαλμάτων. Για περισσότερες πληροφορίες μπορείτε να διαβάσετε τις <a href="https://help.gnome.org/misc/release-notes/3.14/index.html.el" title="Σημειώσεις έκδοσης 3.14" target="_blank">σημειώσεις έκδοσης</a> για όλες τις αλλαγές.

Όλα τα παραπάνω ήταν αποτέλεσμα εργασίας μηνών από πολύ κόσμο της παγκόσμιας κοινότητας και τους ευχαριστούμε όλους για το εξαιρετικό αποτέλεσμα της εργασίας τους.

Από πλευράς ελληνικής κοινότητας, έγινε μια τεράστια προσπάθεια για τη μετάφραση αυτού του κύκλου ανάπτυξης. Σαν κοινότητα συμμετείχαμε στο [Outreach Program for Women](http://gnome.gr/2014/09/18/nea-apo-ti-summetoxi-sto-outreach-program-for-women/ "Νέα από τη συμμετοχή μας στο Outreach Program for Women") που διοργανώθηκε από την παγκόσμια κοινότητα του GNOME. Έχει μεταφραστεί σχεδόν το **100%** στο γραφικό περιβάλλον και το **94%** στην τεκμηρίωση. Απομένει ο έλεγχος μερικών αρχείων,κυρίως στην τεκμηρίωση, ώστε να ενσωματωθούν και να είναι έτοιμα για την έπομενη έκδοση 3.14.1 όπου θα δούμε και στις περισσότερες διανομές.

Θα θέλαμε να ευχαριστήσουμε τα άτομα που συνέβαλαν είτε μια γραμμή είτε περισσότερες στο μεταφραστικό έργο. Αυτά τα άτομα είναι (αλφαβητική σειρά):

  1. Γιάννης Κασκαμανίδης
  2. Δημήτρης Σπίγγος
  3. Ευστάθιος Ιωσηφίδης (συμμετοχή ως μέντορας στο **Outreach Program for Women**)
  4. Εύα Φωτοπούλου (υποψήφια στο **Outreach Program for Women**)
  5. Θάνος Τρυφωνίδης
  6. Μαρία Θουκιδίδου (υποψήφια στο **Outreach Program for Women**)
  7. Μαρία Μαυρίδου ([συμμετοχή](http://gnome.gr/2014/09/18/nea-apo-ti-summetoxi-sto-outreach-program-for-women/ "Νέα από τη συμμετοχή μας στο Outreach Program for Women") στο OPW)
  8. Μαρία Σαβίδη

Επιπλέον θα θέλαμε να ευχαριστήσουμε τους Δημήτρη Σπίγγο και Θάνο Τρυφωνίδη για τον συντονισμό και την ενσωμάτωση των μεταφρασμένων αρχείων στο σύστημα.

Αν θέλετε να [βοηθήσετε](http://gnome.gr/contribute/) και εσείς στο έργο της κοινότητας μας, επικοινωνήστε μαζί μας στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα αλληλογραφίας</a> ή στα [social media](http://gnome.gr/#social_media).
