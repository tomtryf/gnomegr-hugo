---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2010-11-30 15:03:37
guid: http://gnome.gr/?p=391
id: 391
permalink: /2010/11/30/%cf%83%cf%85%ce%bc%ce%bc%ce%b5%cf%84%ce%bf%cf%87%ce%ae-%ce%bc%ce%b1%ce%b8%ce%b7%cf%84%cf%8e%ce%bd-%cf%83%cf%84%ce%bf-google-code-in-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-%ce%ad%cf%81%ce%b3%ce%bf-gnome/
title: Συμμετοχή μαθητών στο Google Code In για το έργο GNOME
url: /2010/11/30/%cf%83%cf%85%ce%bc%ce%bc%ce%b5%cf%84%ce%bf%cf%87%ce%ae-%ce%bc%ce%b1%ce%b8%ce%b7%cf%84%cf%8e%ce%bd-%cf%83%cf%84%ce%bf-google-code-in-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-%ce%ad%cf%81%ce%b3%ce%bf-gnome/
---

Ως ελληνική κοινότητα GNOME (<a href="https://www.gnome.gr/" target="_blank">https://www.gnome.gr/</a> ) λαμβάνουμε μέρος στο Google Code In 2010,
  
<a href="http://www.google-melange.com/site/home/site" target="_blank">http://www.google-melange.com/site/home/site</a>
  
Το Google Code In απευθύνεται στους μαθητές γυμνασίου/λυκείου, όπου εκτελούν μικρές εργασίες πάνω σε ελ/λακ, με μικρή αμοιβή για κάθε εργασία ($100, <a href="http://www.google.com/search?client=ubuntu&channel=fs&q=100+dollars+in+euros&ie=utf-8&oe=utf-8" target="_blank">περίπου 76€</a>). Οι εργασίες σχεδιάζονται ώστε να μπορούν να ολοκληρωθούν σε σύντομο χρονικό διάστημα.
  
Μπορείτε να δείτε το σύνολο τον διαθέσιμων εργασιών στη σελίδα
  
<a href="http://www.google-melange.com/gci/program/list_tasks/google/gci2010" target="_blank">http://www.google-melange.com/gci/program/list_tasks/google/gci2010</a>
  
Μπορείτε από την παραπάνω σελίδα να φιλτράρετε ως προς το «tag» “Greek”, για να δείτε τις εργασίες που δήλωσε η ελληνική κοινότητα GNOME. Αυτή τη στιγμή υπάρχουν πέντε εργασίες για μεταφράσεις, και υπάρχει η δυνατότητα για να προσθέσουμε περισσότερες, αν υπάρχει ενδιαφέρον.
  
Για να δείτε τις ελληνικές εργασίες,

  1. πηγαίνετε στο <a href="http://www.google-melange.com/gci/program/list_tasks/google/gci2010" target="_blank">http://www.google-melange.com/gci/program/list_tasks/google/gci2010</a>
  2. (περιμένετε να φορτωθεί η σελίδα διότι φορτώνει όλες τις διαθέσιμες εργασίες)
  3. εκεί που αναφέρει Tag γράφετε «Greek» και πατάτε Enter για να φιλτράρετε ως προς τις (πέντε) ελληνικές εργασίες.

Το επόμενο βήμα είναι να γραφτείτε στο σύστημα και να δηλώσετε ότι θέλετε να λάβετε μέρος σε μια από τις εργασίες.
  
Για κάθε ερώτηση γραφτείτε στη λίστα gnome.gr από το <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">http://lists.gnome.gr/listinfo.cgi/team-gnome.gr</a>
  
Είναι μια εξαιρετική ευκαιρία να έχουμε πολλά άτομα να λάβουν μέρος στις εργασίες. Η ρουμανική ομάδα του GNOME
  
ολοκλήρωσε ήδη τις πέντε πρώτες εργασίες τους και τώρα προχωρούν σε περισσότερες.
