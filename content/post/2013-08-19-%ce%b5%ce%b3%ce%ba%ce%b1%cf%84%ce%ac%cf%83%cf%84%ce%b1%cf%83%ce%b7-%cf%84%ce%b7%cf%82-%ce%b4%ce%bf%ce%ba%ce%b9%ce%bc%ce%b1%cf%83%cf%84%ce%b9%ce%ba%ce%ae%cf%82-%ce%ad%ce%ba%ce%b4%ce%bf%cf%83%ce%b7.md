---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/7793655042721957670
blogger_permalink:
  - /2013/08/virtual-box.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-08-19 19:01:00
guid: http://gnome.gr/2013/08/19/%ce%b5%ce%b3%ce%ba%ce%b1%cf%84%ce%ac%cf%83%cf%84%ce%b1%cf%83%ce%b7-%cf%84%ce%b7%cf%82-%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%b1%cf%83%cf%84%ce%b9%ce%ba%ce%ae%cf%82-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7/
id: 2917
permalink: /2013/08/19/%ce%b5%ce%b3%ce%ba%ce%b1%cf%84%ce%ac%cf%83%cf%84%ce%b1%cf%83%ce%b7-%cf%84%ce%b7%cf%82-%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%b1%cf%83%cf%84%ce%b9%ce%ba%ce%ae%cf%82-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7/
title: Εγκατάσταση της δοκιμαστικής έκδοσης 13.10 σε Virtual Box
url: /2013/08/19/%ce%b5%ce%b3%ce%ba%ce%b1%cf%84%ce%ac%cf%83%cf%84%ce%b1%cf%83%ce%b7-%cf%84%ce%b7%cf%82-%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%b1%cf%83%cf%84%ce%b9%ce%ba%ce%ae%cf%82-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7/
---

Για όσους δεν θέλετε να πειράξετε τον υπολογιστή σας, η λύση του Virtual Box είναι ιδανική. Στην δοκιμαστική έκδοση 13.10 (<a href="http://cdimage.ubuntu.com/ubuntu-gnome/daily-live/current/" target="1">που μπορείτε να κατεβάσετε από εδώ</a>), πιθανό να αντιμετωπίσετε το σφάλμα για πυρήνα pae. Αυτό μπορεί να λυθεί ως εξής:

1. Μετακινηθείτε στο **settings > system > processor**. Εκεί θα βρείτε το κουτάκι **«Enable PAE/NX»**. Πρέπει να το υποστηρίζει δε ο επεξεργαστής του υπολογιστή σας.

Τώρα μπορείτε να τρέξετε και να εγκαταστήσετε την 13.10.

2. Προαιρετικά, εγκαταστήστε το extension pack του Virtual Box (<a href="https://www.virtualbox.org/wiki/Downloads" target="1">θα το βρείτε εδώ</a>) καθώς και το πακέτο «dkms» (στο μηχάνημα host).