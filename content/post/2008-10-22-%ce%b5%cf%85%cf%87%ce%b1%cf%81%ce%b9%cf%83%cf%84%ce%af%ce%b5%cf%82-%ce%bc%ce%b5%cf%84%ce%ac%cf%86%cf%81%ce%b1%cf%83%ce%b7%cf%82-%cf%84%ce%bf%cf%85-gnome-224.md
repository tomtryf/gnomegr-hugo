---
author: simosx
categories:
  - Ανακοινώσεις κοινότητας
date: 2008-10-22 15:25:12
guid: http://gnome.gr/?p=33
id: 33
permalink: /2008/10/22/%ce%b5%cf%85%cf%87%ce%b1%cf%81%ce%b9%cf%83%cf%84%ce%af%ce%b5%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-224/
title: Ευχαριστίες μετάφρασης του GNOME 2.24
url: /2008/10/22/%ce%b5%cf%85%cf%87%ce%b1%cf%81%ce%b9%cf%83%cf%84%ce%af%ce%b5%cf%82-%ce%bc%ce%b5%cf%84%ce%ac%cf%86%cf%81%ce%b1%cf%83%ce%b7%cf%82-%cf%84%ce%bf%cf%85-gnome-224/
---

Το GNOME είναι το βασικό γραφικό περιβάλλον σε διανομές όπως Ubuntu Linux και Fedora Linux.

Για τις πληροφορίες έκδοσης (release notes) του GNOME 2.24 και για το τι νέο προσφέρει, δείτε
  
 <a href="http://library.gnome.org/misc/release-notes/2.24/index.html" target="_blank">http://library.gnome.org/misc/release-notes/2.24/index.html</a>

Έχει μεταφραστεί το 30% (βελτίωση ποσοστού κατά 8% από προηγούμενη έκδοση) της τεκμηρίωσης και το 93% (από 96% στην προηγούμενη έκδοση) των μηνυμάτων του συνολικού γραφικού περιβάλλοντος του βασικού GNOME 2.24.
  
Δείτε τα [στατιστικά μετάφρασης του GNOME 2.24](http://l10n.gnome.org/languages/el/gnome-2-24).

Για την έκδοση αυτή (Απρίλιος 08 μέχρι Οκτώβριος 08) βοήθησαν, για την πλήρη μετάφραση ή την ενημέρωση υπάρχουσας μετάφρασης,

  1. Κώστας Παπαδήμας 8 πακέτα
  2. Νίκος Χαρωνιτάκης 14 πακέτα
  3. Νίκος Αγιαννιώτης 9 πακέτα
  4. Αθανάσιος Λευτέρης 7 πακέτα
  5. Γιάννης Κασκαμανίδης 1 πακέτο
  6. Σίμος Ξενιτέλλης 7 πακέτα
  7. Rizitis 2 πακέτα
  8. Νίκος Παράσχου 1 πακέτο
  9. Tom Tryfonidis 1 πακέτο
 10. Αντώνης Αντωνούλας 1 πακέτο
 11. Πιερρός Παπαδέας 4 πακέτα
 12. Ηλίας Μακρής 6 πακέτα
 13. Φώτης Τσάμης 2 πακέτα
 14. Κωνσταντίνος Κουράτορας 4 πακέτα
 15. Παντελής Χατζηπαντελής 1 πακέτο
 16. Γιάννης Κατσαμπίρης 1 πακέτο

Οι Κώστας Παπαδήμας, Νίκος Χαρωνιτάκης και Σίμος Ξενιτέλλης βοήθησαν στην προώθηση των πακέτων στο έργο GNOME.

Η ελληνική ομάδα του GNOME βοήθησε και προγραμματιστικά το έργο GNOME,

  1. [Bug 550062 – Small update in gdk/gdkkeysyms.h](http://bugzilla.gnome.org/show_bug.cgi?id=550062)
  2. <a href="http://bugzilla.gnome.org/show_bug.cgi?id=550676" target="_blank">Bug 550676 – Memory leak, update keyboard layout data structure</a>
  3. <a href="http://bugzilla.gnome.org/show_bug.cgi?id=555625" target="_blank">Bug 555625 – Updated gtk_compose_seqs_compact table (gtkimcontextsimpleseqs.h)</a>
  4. <a href="http://bugzilla.gnome.org/show_bug.cgi?id=555000" target="_blank">Bug 555000 – Wrong treatment on non-spacing marks dead keys in GtkIMContextSimple</a>
  5. <a href="http://bugzilla.gnome.org/show_bug.cgi?id=554506" target="_blank">Bug 554506 – combining diacritics broken, became deadkeys</a>
  6. <a href="http://bugzilla.gnome.org/show_bug.cgi?id=554192" target="_blank">Bug 554192 – double press on the «circumflex» dead key (standard french 105 keyboard) no longer produces the «^» character</a>

Για την αμέσως προηγούμενη έκδοση του GNOME, 2.22, βοήθησαν οι
  
 <a href="http://wiki.gnome.gr/doku.php?id=translation:credits222" target="_blank">http://wiki.gnome.gr/doku.php?id=translation:credits222</a>

Η γενική σελίδα με το μεταφραστικό έργο είναι
  
<a href="http://l10n.gnome.org/languages/el/gnome-2-24" target="_blank">http://l10n.gnome.org/languages/el/gnome-2-24</a>

Μπορείτε να δείτε τις λεπτομέρειες των ελληνικών μεταφράσεων καθώς εισάγονται στο GNOME από τη σελίδα
  
<a href="https://www.google.com/reader/shared/user/03486055522304083959/label/gnome-el" target="_blank">https://www.google.com/reader/shared/user/03486055522304083959/label/gnome-el</a>

Οι διανομές όπως Ubuntu, Fedora, θα πάρουν το GNOME 2.24.1, το οποίο έχει οριστικοποιηθεί αυτές τις μέρες.

Η ελληνική ομάδα του έργου GNOME έχει 57 μέλη. Σε κάθε νέα έκδοση του GNOME, τα μέλη που έχουν τη δυνατότητα βοηθούν στην μετάφραση. Στην έκδοση 2.24 βοήθησαν 16 μέλη.
  
Η συμμετοχή είναι ανοιχτή, και αρκεί η εγγραφή σας στη λίστα team@gnome.gr,
  
<a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">http://lists.gnome.gr/listinfo.cgi/team-gnome.gr</a>

Τεχνικές πληροφορίες για τη μετάφραση, δείτε στο Wiki του gnome.gr,
  
<a href="http://wiki.gnome.gr/" target="_blank">http://wiki.gnome.gr/</a>

Ο δικτυακός τόπος του ελληνικού έργου GNOME είναι
  
<a href="https://www.gnome.gr/" target="_blank">https://www.gnome.gr/</a>
