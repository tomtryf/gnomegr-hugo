---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2013-03-27 23:53:20
guid: http://gnome.gr/?p=2009
id: 2009
permalink: /2013/03/27/gnome-3-8-released/
tags:
  - gnome
  - gnome 3.8
title: Κυκλοφόρησε η έκδοση GNOME 3.8
url: /2013/03/27/gnome-3-8-released/
---

Το έργο GNOME ανακοινώνει την έκδοση 3.8. Η τελευταία έκδοση του GNOME 3 έρχεται με μεγάλες νέες αλλαγές, νέες εφαρμογές και μια σειρά από μικρότερες διορθώσεις σφαλμάτων και βελτιώσεων. Εκ της υπεύθυνης ομάδας της έκδοσης 3.8, ο Matthias Clasen δήλωσε, «Είμαστε ενθουσιασμένοι με την τελευταία έκδοση του GNOME 3. Είναι μια ισχυρή έκδοση καθώς είναι μια μεγάλη βελτίωση στην εμπειρία χρήσης του GNOME 3. Θα θέλαμε να ευχαριστήσουμε όλη την κοινότητα GNOME για την αφοσίωσή τους και την σκληρή δουλειά».

Τι περιέχει το GNOME 3.8:

* Επανασχεδιασμένη προβολή εκκίνησης εφαρμογών, που κάνει ευκολότερη από ποτέ την εύρεση εφαρμογών.
  
* Αναβαθμισμένη αναζήτηση, με αναβαθμισμένη προβολή αποτελεσμάτων αναζήτησης.
  
* Νέες ρυθμίσεις ιδιωτικότητας που σας επιτρέπουν να ελέγχετε ποιοι έχουν πρόσβαση στο περιεχόμενο του υπολογιστή σας.
  
* Νέα κλασσική κατάσταση για αυτούς που προτιμούν μια πιο παραδοσιακή εμπειρία χρήσης του περιβάλλοντος.
  
* Βελτιωμένη απόδοση εφέ με αποτέλεσμα την πιο στρωτή μετάβαση και αλλαγή μεγέθους παραθύρων.
  
* Μια νέα εφαρμογή Ρολογιών, που παρέχει παγκόσμια ρολόγια-ώρα για διαφορετικές ζώνες ώρας όπως και ξυπνητήρια, χρονόμετρα κλπ.
  
* Αναβάθμιση ρυθμίσεων με 4 νέους πίνακες ρυθμίσεων και μεγάλες ενημερώσεις σε πολλές επιλογές.
  
* Πολλές ενημερώσεις εφαρμογών του GNOME, συμπεριλαμβανομένων και μεγάλων βελτιώσεων στην απόδοση του Web, ενσωματώσεις UI στην εφαρμογή Documents και νέα κατάσταση επεξεργασίας Επαφών.

Μπορείτε να βρείτε περισσότερα για τις βελτιώσεις του GNOME 3.8 στις <a href="https://help.gnome.org/misc/release-notes/3.8/" target="_blank">σημειώσεις της έκδοσης</a>.

<div id='gallery-1' class='gallery galleryid-2009 gallery-columns-3 gallery-size-thumbnail'>
  <figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='https://www.gnome.gr/2013/03/27/gnome-3-8-released/applications-view/'><img width="150" height="84" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2013/03/applications-view.png" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-2010" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-1-2010'> Προβολή Εφαρμογών </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='https://www.gnome.gr/2013/03/27/gnome-3-8-released/clocks/'><img width="150" height="84" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2013/03/clocks.png" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-2011" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-1-2011'> Ρολόγια </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='https://www.gnome.gr/2013/03/27/gnome-3-8-released/documents-navigation/'><img width="150" height="84" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2013/03/documents-navigation.png" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-2012" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-1-2012'> Documents </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='https://www.gnome.gr/2013/03/27/gnome-3-8-released/goa-add-account/'><img width="150" height="84" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2013/03/goa-add-account.png" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-2013" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-1-2013'> Online Accounts </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='https://www.gnome.gr/2013/03/27/gnome-3-8-released/search/'><img width="150" height="84" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2013/03/search.png" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-2014" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-1-2014'> Αναζήτηση </figcaption></figure><figure class='gallery-item'> 
  
  <div class='gallery-icon landscape'>
    <a href='https://www.gnome.gr/2013/03/27/gnome-3-8-released/settings-power/'><img width="150" height="84" src="https://static.gnome.org/gnome-gr/uploads/sites/7/2013/03/settings-power.png" class="attachment-thumbnail size-thumbnail" alt="" aria-describedby="gallery-1-2015" /></a>
  </div><figcaption class='wp-caption-text gallery-caption' id='gallery-1-2015'> Ρυθμίσεις </figcaption></figure>
</div>

Οι συνεργάτες του GNOME ήδη καλωσόρισαν την νέα έκδοση. Ο Stefano Zacchiroli, Debian Project Leader, δήλωσε «Χάρη στο GNOME, το προεπιλεγμένο γραφικό περιβάλλον μας εδώ και πολλά χρόνια, είμαστε σε θέση να προσφέρουμε στους χρήστες μας ένα δωρεάν παραγωγικό περιβάλλον το οποίο είναι οπτικά συμπαθητικό αλλά και εύκολο στην χρήση. Εύχομαι στην κοινότητα του GNOME τα καλύτερα για την έκδοση GNOME 3.8, την οποία θέλουμε να έχουμε στην υπό ανάπτυξη έκδοσή μας».

«Είμαστε πραγματικά ενθουσιασμένοι με την έκδοση 3.8», δήλωσαν με ανακοίνωση από την Igalia, «εν μέρη γιατί το Web, ο φυλλομετρητής του GNOME, προσφέρει το σύστημα υποστήριξης WebKit2, κάτι που εμείς στην Igalia δουλεύουμε εδώ και χρόνια. Το WebKid2 προσφέρει τελευταίες τεχνολογίες στην πλατφόρμα μας, με αυξημένη ανταπόκριση, ασφάλεια και σταθερότητα, κάνοντας το Web 3.8 και όλλες τις εφαρμογές που χρησιμοποιούν WebKit πρεισσότερο ευχάριστες και ενδιαφέρον στη χρήση».

Δείτε το <a href="http://www.gnome.org/press/2013/03/gnome-community-releases-gnome-3-8/" target="_blank">δελτίο τύπου</a> για την έκδοση GNOME 3.8.
