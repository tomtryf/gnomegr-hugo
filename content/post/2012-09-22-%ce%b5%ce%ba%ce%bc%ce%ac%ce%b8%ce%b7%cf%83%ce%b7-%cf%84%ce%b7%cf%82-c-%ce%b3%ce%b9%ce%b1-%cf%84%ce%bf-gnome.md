---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2012-09-22 23:36:34
guid: http://gnome.gr/?p=1775
id: 1775
permalink: /2012/09/22/%ce%b5%ce%ba%ce%bc%ce%ac%ce%b8%ce%b7%cf%83%ce%b7-%cf%84%ce%b7%cf%82-c-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-gnome/
tags:
  - gnome
title: Μάθετε C με τη βοήθεια του GNOME
url: /2012/09/22/%ce%b5%ce%ba%ce%bc%ce%ac%ce%b8%ce%b7%cf%83%ce%b7-%cf%84%ce%b7%cf%82-c-%ce%b3%ce%b9%ce%b1-%cf%84%ce%bf-gnome/
---

Το <a href="https://live.gnome.org/GnomeUniversity" target="_blank"><strong>GNOME University</strong> </a>είναι μια προσπάθεια του <a href="https://live.gnome.org/ChristianHergert" target="_blank">Christian Hergert</a> να διδάξει C σε όσους θέλουν να βοηθήσουν στην ανάπτυξη του GNOME.Δεν χρειάζεται να έχετε γνώσεις C καθώς τα μαθήματα ξεκινούν από το μηδέν με τα απολύτως βασικά πράγματα.
  
Ο Christian θα γράφει μαθήματα κάθε βδομάδα που θα περιέχουν ασκήσεις στις οποίες θα δουλεύουν οι ενδιαφερόμενοι.Αφού ολοκληρωθούν τα βασικά θα ακολουθήσουν προχωρημένα θέματα και εισαγωγή στα API του GNOME (στα δύσκολα θα υπάρχουν screencasts για καλύτερη κατανόηση).Μέχρι στιγμής το υλικό των πρώτων μαθημάτων και ασκήσεων υπάρχει στο <a href="https://github.com/chergert/gnome-desktop-programming" target="_blank">github</a>.

&nbsp;

Αν θέλετε να μάθετε C επικοινωνήστε με τον <a href="https://live.gnome.org/ChristianHergert" target="_blank">Christian Hergert</a> μέσω mail δηλώνοντας τα εξής:

  * Την εμπειρία σας στον προγραμματισμό (αν δεν έχετε γράψτε το,μη ντραπείτε)
  * Σε ποιον τομέα του GNOME ενδιαφέρεστε να ασχοληθείτε (GTK,επέκταση του GNOME Shell,πολυμέσα κλπ)
  * Ζώνη ώρας (π.χ GMT +2 για Ελλάδα)
  * Τη διανομή που χρησιμοποιείτε και την έκδοση του GNOME (για να σας βοηθήσουν με την εγκατάσταση των απαραίτητων προγραμμάτων προγραμματισμού)

&nbsp;

Είναι μια καλή αρχή αν θέλετε να ξεκινήσετε με τον προγραμματισμό ή αν έχετε γνώσεις και θέλετε να βοηθήσετε το έργο του GNOME.Όσοι δηλώσετε συμμετοχή επικοινωνήστε μαζί μας στη <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα ταχυδρομείου</a> γράφοντας μας τις εμπειρίες σας.