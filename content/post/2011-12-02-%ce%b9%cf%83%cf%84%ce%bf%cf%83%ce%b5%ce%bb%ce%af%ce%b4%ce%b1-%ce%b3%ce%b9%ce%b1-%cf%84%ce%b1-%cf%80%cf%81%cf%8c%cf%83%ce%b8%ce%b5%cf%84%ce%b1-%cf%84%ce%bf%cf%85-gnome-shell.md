---
author: thanos
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2011-12-02 17:42:09
guid: http://gnome.gr/?p=1114
id: 1114
permalink: /2011/12/02/%ce%b9%cf%83%cf%84%ce%bf%cf%83%ce%b5%ce%bb%ce%af%ce%b4%ce%b1-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b1-%cf%80%cf%81%cf%8c%cf%83%ce%b8%ce%b5%cf%84%ce%b1-%cf%84%ce%bf%cf%85-gnome-shell/
title: Ιστοσελίδα για τα πρόσθετα του Gnome Shell
url: /2011/12/02/%ce%b9%cf%83%cf%84%ce%bf%cf%83%ce%b5%ce%bb%ce%af%ce%b4%ce%b1-%ce%b3%ce%b9%ce%b1-%cf%84%ce%b1-%cf%80%cf%81%cf%8c%cf%83%ce%b8%ce%b5%cf%84%ce%b1-%cf%84%ce%bf%cf%85-gnome-shell/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/12/gnome-shell-extensions.png" alt="gnome-shell-extensions" title="gnome-shell-extensions" class="alignnone size-full wp-image-1121 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2011/12/gnome-shell-extensions.png)

Το [Gnome Shell Extensions](https://extensions.gnome.org/) πρόκειται για ένα νέο project της κοινότητας του GNOME που σκοπό έχει την εύκολη εγκατάσταση των προσθέτων μας στο Gnome Shell.Τα πρόσθετα που είναι διαθέσιμα στη σελίδα μπορούμε με ένα κλικ να τα κάνουμε εγκατάσταση στον υπολογιστή μας και να τα χρησιμοποιήσουμε επί τόπου.

Στη σελίδα «<a href="https://extensions.gnome.org/local/" target="_blank">Installed Extensions</a>» κάνουμε διαχείριση των προσθέτων μας που έχουμε εγκαταστήσει στον υπολογιστή μας.Μπορούμε να απενεργοποιήσουμε ή να διαγράψουμε ένα πρόσθετο χωρίς επιπρόσθετη χρήση προγραμμάτων (βλέπε <a href="http://live.gnome.org/GnomeTweakTool" target="_blank">Gnome Tweak Tool</a>) γρήγορα και εύκολα.

Επίσης η σελίδα επιτρέπει στους προγραμματιστές να ανεβάσουν τα δικά τους πρόσθετα,το μόνο που χρειάζεται είναι να δημιουργήσετε έναν <a href="https://extensions.gnome.org/accounts/register/" target="_blank">λογαριασμό</a> στη σελίδα.

Χρήστες Epiphany,Chromium,Midori ή κάποιου άλλου browser που βασίζεται στο WebKit GTK+ δεν μπορούν να χρησιμοποιήσουν τη σελίδα,λόγω ενός γνωστού bug,που η διόρθωση του θα ενσωματωθεί στην έκδοση 3.4 του GNOME τον ερχόμενο Μάρτιο.

<div class="action_box">
  Η σελίδα «Gnome Shell Extensions» είναι ακόμα σε <strong>alpha στάδιο</strong> οπότε αν βρείτε κάποιο bug καλά είναι να τo <a href="https://bugzilla.gnome.org/enter_bug.cgi?product=website&component=extensions.gnome.org" target="_blank">αναφέρετε</a> για να βοηθήσετε τους προγραμματιστές.
</div>

* * *
