---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/3577288061446522635
blogger_permalink:
  - /2014/09/ubuntu-gnome-1410-beta-2.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2014-09-26 12:20:00
guid: http://gnome.gr/2014/09/26/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%b1%cf%83%cf%84%ce%b9%ce%ba%ce%ae-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-ubuntu-gnome-14-10-beta-2/
id: 2895
permalink: /2014/09/26/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%b1%cf%83%cf%84%ce%b9%ce%ba%ce%ae-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-ubuntu-gnome-14-10-beta-2/
title: Δοκιμαστική έκδοση Ubuntu GNOME 14.10 beta 2
url: /2014/09/26/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%b1%cf%83%cf%84%ce%b9%ce%ba%ce%ae-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-ubuntu-gnome-14-10-beta-2/
---

Η ομάδα <a href="http://distrowatch.com/table.php?distribution=ubuntugnome" target="1">Ubuntu GNOME</a> σας παρουσιάζει την δοκιμαστική έκδοση Ubuntu GNOME Utopic Unicorn Beta 1.

Παρακαλούμε, για περισσότερες πληροφορίες διαβάστε τις σημειώσεις της [έκδοσης beta 2 της 14.10](https://wiki.ubuntu.com/UtopicUnicorn/Beta2/UbuntuGNOME).

Ιστοσελίδα <a href="http://cdimage.ubuntu.com/ubuntu-gnome/releases/14.10/beta-2/" target="1">ΛΗΨΕΩΝ beta 2</a>

**ΣΗΜΕΙΩΣΗ:**

Αυτή είναι η έκδοση Beta 1. Οι εκδόσεις Ubuntu GNOME Beta ΔΕΝ συνίσταται για:

* Τελικούς χρήστες που δεν γνωρίζουν για τα προβλήματα των προ-εκδόσεων   
* Όποιον χρειάζεται ένα σταθερό σύστημα  
* Όποιος δεν αισθάνεται άνετα να καταλήγει με ένα κατεστραμμένο σύστημα  
* Παραγωγικό περιβάλλον με δεδομένα και έργα που χρειάζεται αξιοπιστία

Οι εκδόσεις **Ubuntu GNOME Beta** συνίσταται για:

* Χρήστες που θέλουν να βοηθήσουν στην ανάπτυξη, βρίσκοντας σφάλματα και υποβάλοντάς τα προς διόρθωση  
* Προγραμματιστές του Ubuntu GNOME

**Βοηθήσετε την δοκιμή του Ubuntu GNOME:**  
Διαβάστε στο <a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing" target="1">Ubuntu GNOME Wiki Page</a>.

Ευχαριστούμε που βοηθάτε στην ανάπτυξη του Ubuntu GNOME.