---
author: iosifidis
categories:
  - Εκδηλώσεις
date: 2013-07-31 13:06:30
guid: http://gnome.gr/?p=2087
id: 2087
permalink: /2013/07/31/guadec-2013-ksekina-aurio/
tags:
  - gnome
  - GUADEC
title: Το συνέδριο GUADEC 2013 ξεκινά αύριο
url: /2013/07/31/guadec-2013-ksekina-aurio/
---

<figure id="attachment_2088" style="width: 640px" class="wp-caption aligncenter">[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/07/Hrad_Špilberk_v_Brně_vyrez.jpg" alt="By Jan Symon (Jan Symon) [CC-BY-SA-3.0], via Wikimedia Commons" class="size-full wp-image-2088 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/07/Hrad_Špilberk_v_Brně_vyrez.jpg)<figcaption class="wp-caption-text">By Jan Symon (Jan Symon) [CC-BY-SA-3.0], via Wikimedia Commons</figcaption></figure> 

Μέλη του έργου GNOME μαζεύονται ήδη στο Brno της Τσεχίας για το ετήσιο Ευρωπαϊκό συνέδριο (GUADEC). Η εκδήλωση ξεκινάει αύριο, 1 Αυγούστου. Θα υπάρξουν τέσσερις βασικές μέρες με παρουσιάσεις, συμπεριλαμβανομένων και ομιλιών περί παιχνιδιών στο Linux, το Wayland, GTK+, τεκμηρίωση, σχεδιασμός, LibreOffice και πολλά άλλα. Το πλήρες πρόγραμμα μπορείτε να το βρείτε στην <a href="https://www.guadec.org/" target="_blank">ιστοσελίδα του GUADEC</a>.

Το συνέδριο αυτό είναι η κύρια εκδήλωση της χρονιάς για το έργο GNOME, και όπως κάθε χρόνο, θα είναι μια εκπαιδευτούμε αλλά θα διασκεδάσουμε.

Εάν δεν καταφέρατε να είστε στο GUADEC φέτος, μην στεναχωριέστε. Μπορείτε να δείτε όλη την «δράση» μέσω δικτύου. Θα υπάρχουν εκτενείς αναφορές στο <a href="http://www.gnome.org" target="_blank">gnome.org</a>. Μπορείτε επίσης να ακολουθείτε το συνέδριο στο <a href="https://twitter.com/gnome" target="_blank">Twitter</a>, <a href="https://www.facebook.com/GNOMEDesktop" target="_blank">Facebook</a> και <a href="https://plus.google.com/+gnome/posts" target="_blank">Google Plus</a>. Απλά ακολουθήστε το GNOME, και αναζητείτε τα post με hashtag #guadec.
