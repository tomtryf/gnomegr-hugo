---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/5577180979428718641
blogger_permalink:
  - /2013/09/ubuntu-gnome-1310.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-09-15 12:20:00
guid: http://gnome.gr/2013/09/15/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae-%cf%84%ce%b7%cf%82-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7%cf%82-ubuntu-gnome-13-10/
id: 2915
permalink: /2013/09/15/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae-%cf%84%ce%b7%cf%82-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7%cf%82-ubuntu-gnome-13-10/
title: Δοκιμή της έκδοσης Ubuntu Gnome 13.10
url: /2013/09/15/%ce%b4%ce%bf%ce%ba%ce%b9%ce%bc%ce%ae-%cf%84%ce%b7%cf%82-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7%cf%82-ubuntu-gnome-13-10/
---

Γεια σας,

Καθώς <a href="https://wiki.ubuntu.com/SaucySalamander/ReleaseSchedule" target="1">πλησιάζουμε στην τελική έκδοση της Ubuntu Gnome 13.10</a>, είναι σημαντικό να συνεχίσουμε τις δοκιμές ώστε να εξασφαλίσουμε την επιτυχία της έκδοσης.

Ευχαριστούμε όσους έχουν συνεισφέρει και έχουν βοηθήσει την ομάδα GNOME στην δοκιμή και ανάπτυξη της Ubuntu GNOME 13.10.

Εάν είστε νέοι στην διαδικασία δοκιμών, αυτό είναι πλεονέκτημα για εμάς. Οι πιθανότητες να καταρρεύσει το σύστημα είναι μεγάλες και αυτό είναι που χρειαζόμαστε. Ακούγεται κάπως; Όχι ακριβώς. Όσο πιο σκληρά δοκιμάσετε το σύστημά σας και βρείτε τα σφάλματα, αυτό σημαίνει ότι τόσο καλύτερο γίνεται το σύστημα. Το χρειαζόμαστε αυτό ώστε να γίνει μια καλή σταθερή έκδοση πριν ακόμα κυκλοφορήσει.

Για περισσότερες πληροφορίες δείτε το wiki: <a href="https://wiki.ubuntu.com/UbuntuGNOME/Testing" target="1">Ubuntu GNOME Testing</a>.

Για καθημερινά στιγμιότυπα: <a href="http://cdimage.ubuntu.com/ubuntu-gnome/daily-live/current/" target="1">Saucy Daily Live ISO</a>

Σημειώσεις έκδοσης της Beta 1: <a href="https://wiki.ubuntu.com/SaucySalamander/Beta1/UbuntuGNOME" target="1">Release Notes</a>

Σας ευχαριστούμε που διαλέξατε να δοκιμάσετε την Ubuntu GNOME.