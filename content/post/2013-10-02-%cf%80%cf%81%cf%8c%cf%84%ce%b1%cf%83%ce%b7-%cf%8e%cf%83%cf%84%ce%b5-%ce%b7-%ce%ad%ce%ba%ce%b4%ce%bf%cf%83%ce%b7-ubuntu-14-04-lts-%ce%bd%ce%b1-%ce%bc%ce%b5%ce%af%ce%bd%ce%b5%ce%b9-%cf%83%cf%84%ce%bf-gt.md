---
author: iosifidis
blogger_author:
  - diamond_gr
blogger_blog:
  - www.ubuntugnome.gr
blogger_internal:
  - /feeds/6290440822915831166/posts/default/3535819011092454824
blogger_permalink:
  - /2013/10/ubuntu-1404-lts-gtkgnome-38.html
categories:
  - Ubuntu GNOME
  - Ειδήσεις
date: 2013-10-02 20:07:00
guid: http://gnome.gr/2013/10/02/%cf%80%cf%81%cf%8c%cf%84%ce%b1%cf%83%ce%b7-%cf%8e%cf%83%cf%84%ce%b5-%ce%b7-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-ubuntu-14-04-lts-%ce%bd%ce%b1-%ce%bc%ce%b5%ce%af%ce%bd%ce%b5%ce%b9-%cf%83%cf%84%ce%bf-gt/
id: 2910
image: /wp-content/uploads/sites/7/2013/10/ubuntu_gnome_logo.png
permalink: /2013/10/02/%cf%80%cf%81%cf%8c%cf%84%ce%b1%cf%83%ce%b7-%cf%8e%cf%83%cf%84%ce%b5-%ce%b7-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-ubuntu-14-04-lts-%ce%bd%ce%b1-%ce%bc%ce%b5%ce%af%ce%bd%ce%b5%ce%b9-%cf%83%cf%84%ce%bf-gt/
title: Πρόταση ώστε η έκδοση Ubuntu 14.04 LTS να μείνει στο GTK/GNOME 3.8&#8230;
url: /2013/10/02/%cf%80%cf%81%cf%8c%cf%84%ce%b1%cf%83%ce%b7-%cf%8e%cf%83%cf%84%ce%b5-%ce%b7-%ce%ad%ce%ba%ce%b4%ce%bf%cf%83%ce%b7-ubuntu-14-04-lts-%ce%bd%ce%b1-%ce%bc%ce%b5%ce%af%ce%bd%ce%b5%ce%b9-%cf%83%cf%84%ce%bf-gt/
---

<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2013/10/ubuntu_gnome_logo-300x300.png"  class="img-responsive" />

Ο Sebastien Bacher, μηχανικός λογισμικού στην Canonical, πρόσφατα έστειλε ένα <a href="https://lists.ubuntu.com/archives/ubuntu-desktop/2013-October/004311.html" target="1">μήνυμα</a> στην <a href="https://lists.ubuntu.com/archives/ubuntu-desktop/2013-October/thread.html" target="1">λίστα Ubuntu Desktop</a>, στο οποίο προτείνει η επόμενη έκδοση Ubuntu (14.04 LTS) να χρησιμοποιεί την έκδοση GTK/GNOME 3.8.

Επειδή η επόμενη έκδοση θα είναι LTS και χρειάζεται σταθερότητα. Αναβαθμίζοντας σε έκδοση GNOME 3.10 ή 3.12 πιθανό να βρείτε πολλά σφάλματα τα οποία να μην μπορούν να διορθωθούν σύντομα διότι οι προγραμματιστές του Ubuntu να δουλεύουν για τα Ubuntu Touch/phone. Αυτό επίσης σημαίνει ότι η έκδοση 14.04 θα μείνει πίσω 2 εκδόσεις GNOME πράγμα που θα επηρεάσει το Ubuntu GNOME, τους χρήστες που συνεισφέρουν στο GNOME όπως επίσης και μερικές εφαρμογές που απαιτούν τελευταίες τεχνολογίες GNOME.

Το μήνυμα του Sebastien Bacher είναι μια πρόσκληση σε συζήτηση περισσότερο παρά μια τελική απόφαση, αλλά αποτελεί μια επιλογή.

Εσάς ποια είναι η γνώμη σας;