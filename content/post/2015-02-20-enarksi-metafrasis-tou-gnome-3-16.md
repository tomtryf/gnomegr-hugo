---
author: iosifidis
categories:
  - Ανακοινώσεις κοινότητας
  - Ειδήσεις
date: 2015-02-20 22:07:13
guid: http://gnome.gr/?p=3009
id: 3009
permalink: /2015/02/20/enarksi-metafrasis-tou-gnome-3-16/
tags:
  - '3.16'
  - gnome
  - μεταφράσεις
title: Έναρξη μετάφρασης του GNOME 3.16
url: /2015/02/20/enarksi-metafrasis-tou-gnome-3-16/
---

[<img src="https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/l10n-300x172.png" alt="" class="aligncenter size-medium wp-image-2734 img-responsive " />](https://static.gnome.org/gnome-gr/wp-content/uploads/sites/7/2014/09/l10n.png)

Η νέα έκδοση 3.16, σύμφωνα με την <a href="https://wiki.gnome.org/ThreePointFifteen" target="_blank">ιστοσελίδα wiki</a>, θα είναι έτοιμη στις 25 Μαρτίου 2015.

Αυτή τη στιγμή τα στατιστικά μας είναι:

**Γραφική διεπαφή** 99% μεταφρασμένη, 295 strings που θέλουν αναθεώρηση
  
και 205 strings που θέλουν μετάφραση.

**Τεκμηρίωση** 96% μεταφρασμένη, 463 strings που θέλουν αναθεώρηση και 426
  
strings που θέλουν μετάφραση.

Όσοι από εσάς θέλετε να βοηθήσετε να ολοκληρωθεί η μετάφραση, μπορείτε να ακολουθήσετε τα βήματα που περιγράφονται στην <a href="http://gnome.gr/contribute/" target="_blank">ιστοσελίδα συμμετοχής</a> ή να <a href="https://www.youtube.com/watch?v=sH-STvL82-Y" target="_blank">δείτε το βίντεο</a>.

Για οποιαδήποτε απορία έχετε, μπορούμε να σας βοηθήσουμε, είτε στο <a href="https://www.facebook.com/gnomegr" target="_blank">facebook</a>, <a href="https://plus.google.com/communities/101177026822277687297" target="_blank">google +</a>, <a href="http://gnome.gr/contact/irc-gnome-el/" target="_blank">IRC</a> (επιμείνετε λίγο εκεί) ή καλύτερα στην <a href="http://lists.gnome.gr/listinfo.cgi/team-gnome.gr" target="_blank">λίστα αλληλογραφίας</a>.
