# br.gnome.org

Brazilian GNOME community website

## Testando

Para testar o site localmente, em seu próprio computador, faça o seguinte:

- Instale o [Hugo][Link para o Hugo]
    - Baixe o [tarball][Pagina de download do tarball do Hugo]
    - Descompacte o arquivo tar.gz
        - Exemplo: `tar -xvzf ~/Downloads/hugo_X.Y_Linux-64bit.tgz`
    - Copie o arquivo executável hugo para `/home/seu-usuario/bin` (ou outra pasta que esteja no PATH)
    - Verifique a instalação com o comando `hugo version`
- Na pasta raiz do projeto, execute o comando `hugo server`
- Abra o navegador em `localhost:1313/br-gnome-org`

[Link para o Hugo]: https://gohugo.io/getting-started/installing
[Pagina de download do tarball do Hugo]: https://github.com/gohugoio/hugo/releases

